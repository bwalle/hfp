#!/bin/sh

NAME=hfp
RELEASE=`grep VERSION_STRING CMakeLists.txt |head -1|sed -e 's/.*"\(.*\)\".*/\1/g'`
hg archive -X dist.sh -t tbz2 ${NAME}-${RELEASE}.tar.bz2

