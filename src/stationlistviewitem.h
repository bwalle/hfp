/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef STATIONLISTVIEWITEM_H
#define STATIONLISTVIEWITEM_H

#include <QtGui>
#include <Qt3Support>

#include "data/transmission.h"

class StationListViewItem : public Q3ListViewItem
{
    public:
        StationListViewItem(Q3ListView* parent, const Transmission& transmission)
            : Q3ListViewItem(parent)
            , m_transmission(transmission)
            , m_beginTime(m_transmission.getBeginTimeAsString())
            , m_endTime(m_transmission.getEndTimeAsString())
        {}

        QString text(int column) const;
        int compare(Q3ListViewItem* i, int col, bool ascending) const;

        Transmission getTransmission() const;

    private:
        Q_DISABLE_COPY(StationListViewItem)
        Transmission m_transmission;
        QString m_beginTime;
        QString m_endTime;
};

#endif /* STATIONLISTVIEWITEM_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
