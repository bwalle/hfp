/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef STATIONLIST_H
#define STATIONLIST_H

#include <QtGui>
#include <Qt3Support>

#include "data/transmissiontable.h"

class CentralWidget;

class StationList : public Q3ListView
{
    Q_OBJECT

    public:
        ~StationList();

    public:
        StationList(QWidget* parent, CentralWidget* central);
        QString getCurrentEntriesForPrint() const;
        QString getTextForExport() const;

    public slots:
        void updateView();
        void loadTransmissionTable();
        void mouseButtonClickedHandler(int button, Q3ListViewItem* item, const QPoint & pos, int c);

    protected:
        void saveSettings();
        void restoreSettings();

    private:
        Q_DISABLE_COPY(StationList)
        TransmissionTable* m_transmissions;
        CentralWidget* m_centralWidget;

};


#endif /* STATIONLIST_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
