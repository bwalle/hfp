/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef FILTERPANEL_H
#define FILTERPANEL_H

#include <QtGui>
#include <Qt3Support>

class CentralWidget;

class FilterPanel : public Q3Frame
{
    Q_OBJECT

    public:
        friend Q3TextStream& operator<<(Q3TextStream& ts, const FilterPanel& panel);
        friend Q3TextStream& operator>>(Q3TextStream& ts, FilterPanel& panel);

    public:
        FilterPanel(QWidget* parent, CentralWidget* centralWidget);
        virtual ~FilterPanel();

    protected slots:
        void updateTimeEdit(int item);
        void updateFilter();
        void updateSpinner(bool on);
        void save();
        void restore();
        void clean();

    protected:
        void saveSettings(const QString& name);
        void restoreSettings(const QString& name);

    signals:
        void filterChanged();

    private:
        Q_DISABLE_COPY(FilterPanel)
        CentralWidget*  m_centralWidget;
        QCheckBox*      m_germanCheckBox;
        QCheckBox*      m_englishCheckBox;
        QCheckBox*      m_russianCheckBox;
        QCheckBox*      m_frenchCheckBox;
        QCheckBox*      m_spanishCheckBox;
        QCheckBox*      m_turkishCheckBox;
        QComboBox*      m_timeCombo;
        Q3TimeEdit*      m_timeEdit;
        QCheckBox*      m_toleranceCheckBox;
        QSpinBox*       m_toleranceSpinner;
        QLineEdit*      m_countryEdit;
        QCheckBox*      m_countryNegateCheckBox;
        QLineEdit*      m_stationEdit;
        QCheckBox*      m_statNegateCheckBox;
        QLineEdit*      m_frequencyEdit;
        QCheckBox*      m_frequNegateCheckBox;

        QToolButton*    m_applyButton;
        QToolButton*    m_saveButton;
        QToolButton*    m_restoreButton;
        QToolButton*    m_cleanButton;
};

Q3TextStream& operator<<(Q3TextStream& ts, const FilterPanel& panel);
Q3TextStream& operator>>(Q3TextStream& ts, FilterPanel& panel);

#endif /* FILTERPANEL_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
