/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QtGui>
#include <Qt3Support>

#include "worldclock.h"

// -------------------------------------------------------------------------------------------------
WorldClock::WorldClock(QWidget* parent) : Q3Frame(parent)
{
    Q3HBoxLayout* layout = new Q3HBoxLayout(this, 0,20);

    m_dateLCD = new QLCDNumber(10, this);
    m_dateLCD->setSegmentStyle(QLCDNumber::Flat);
    m_worldTimeLCD = new QLCDNumber(8, this);
    m_worldTimeLCD->setSegmentStyle(QLCDNumber::Flat);

    m_dateLCD->setFrameStyle(Q3Frame::NoFrame);
    m_dateLCD->setFixedHeight(24);
    m_dateLCD->setFixedWidth(130);
    m_worldTimeLCD->setFrameStyle(Q3Frame::NoFrame);
    m_worldTimeLCD->setFixedHeight(24);
    m_worldTimeLCD->setFixedWidth(100);

    layout->addWidget(m_worldTimeLCD);
    layout->addWidget(m_dateLCD);

    updateTime();

    m_updateTimer = new QTimer(this);
    connect(m_updateTimer, SIGNAL(timeout()), SLOT(updateTime()));
    m_updateTimer->start(500);
}


// -------------------------------------------------------------------------------------------------
void WorldClock::updateTime()
{
    QDateTime current = QDateTime::currentDateTime(Qt::UTC);
    m_worldTimeLCD->display(current.toString("hh:mm:ss"));
    m_dateLCD->display(current.toString("yyyy-MM-dd"));
}


// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
