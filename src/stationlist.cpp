/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <stdexcept>

#include <QtGui>
#include <Qt3Support>

#include "hfp.h"
#include "settings.h"
#include "global.h"
#include "stationlist.h"
#include "centralwidget.h"
#include "data/transmissiontable.h"
#include "data/transmissionreader.h"
#include "stationlistviewitem.h"

using std::exception;

// -------------------------------------------------------------------------------------------------
StationList::StationList(QWidget* parent, CentralWidget* central)
    : Q3ListView(parent)
    , m_transmissions(0)
    , m_centralWidget(central)
{
    Settings& set = Settings::set();
    int x;

    addColumn(tr("Start"),      (x = set.readNumEntry("GUI/stationListColumn0Width")) ? x : 50 );
    addColumn(tr("End"),        (x = set.readNumEntry("GUI/stationListColumn1Width")) ? x : 50);
    addColumn(tr("Language"),   (x = set.readNumEntry("GUI/stationListColumn2Width")) ? x : 70);
    addColumn(tr("ITU"),        (x = set.readNumEntry("GUI/stationListColumn3Width")) ? x : 50);
    addColumn(tr("Station"),    (x = set.readNumEntry("GUI/stationListColumn4Width")) ? x : 150);
    addColumn(tr("Frequencies"),(x = set.readNumEntry("GUI/stationListColumn5Width")) ? x : 250);

    setResizeMode(LastColumn);
    header()->setMovingEnabled(false);

    setAllColumnsShowFocus(true);
    setShowSortIndicator(true);

    connect(this, SIGNAL(mouseButtonClicked(int, Q3ListViewItem*, const QPoint&, int)),
        SLOT(mouseButtonClickedHandler(int, Q3ListViewItem*, const QPoint&, int)));

    restoreSettings();
}


// -------------------------------------------------------------------------------------------------
StationList::~StationList()
{
    saveSettings();

    delete m_transmissions;
}


// -------------------------------------------------------------------------------------------------
void StationList::saveSettings()
{
    Settings::set().writeEntry("GUI/stationListColumn0Width", columnWidth(0));
    Settings::set().writeEntry("GUI/stationListColumn1Width", columnWidth(1));
    Settings::set().writeEntry("GUI/stationListColumn2Width", columnWidth(2));
    Settings::set().writeEntry("GUI/stationListColumn3Width", columnWidth(3));
    Settings::set().writeEntry("GUI/stationListColumn4Width", columnWidth(4));
    Settings::set().writeEntry("GUI/stationListColumn5Width", columnWidth(5));
    Settings::set().writeEntry("GUI/TransmissionSortColumn", sortColumn());
    Settings::set().writeEntry("GUI/SettingsStored", true);
    Settings::set().writeEntry("GUI/TransmissionSortColumnOrderAsc", sortOrder() == Qt::Ascending);
}


// -------------------------------------------------------------------------------------------------
void StationList::restoreSettings()
{
    if (Settings::set().readBoolEntry("GUI/SettingsStored")) {
        setSortColumn(Settings::set().readNumEntry("GUI/TransmissionSortColumn"));
        setSortOrder( Settings::set().readBoolEntry("GUI/TransmissionSortColumnOrderAsc") ?
            Qt::Ascending : Qt::Descending );

    } else
        setSortColumn(0);
}


// -------------------------------------------------------------------------------------------------
void StationList::updateView()
{
    if (m_transmissions) {
        clear();

        for (size_t i = 0; i < m_transmissions->size(); i++) {
            Transmission t = m_transmissions->getTransmission(i);

            TransmissionFilter& filter = m_centralWidget->getTransmissionFilter();

            if (filter.accept(t))
                (void) new StationListViewItem( this, t);
        }

        sort();
        setCurrentItem(firstChild());
        setSelected(firstChild(), true);
    }
}


// -------------------------------------------------------------------------------------------------
void StationList::loadTransmissionTable()
{
    try {
        delete m_transmissions;

        if (!QDir(Settings::set().readEntry("General/Datadir")).exists())
            return;

        m_transmissions = new TransmissionTable();
        TransmissionReader reader;
        reader.addAllTransmissions( Settings::set().readEntry("General/Datadir"), *m_transmissions);

        updateView();
    } catch (const exception& ex) {
        QMessageBox::warning(this, "HFP",
            tr("An unknown error occured while reading the data files:\n\n%1").arg(ex.what()),
            QMessageBox::Ok, QMessageBox::NoButton);
    }
}


// -------------------------------------------------------------------------------------------------
void StationList::mouseButtonClickedHandler(int button, Q3ListViewItem* item,
                                            const QPoint & pos, int c)
{
    if (item && (button & Qt::MidButton)) {
        QString text = item->text(c);

        QClipboard* clip = QApplication::clipboard();
        clip->setText(text, QClipboard::Clipboard);
        if (clip->supportsSelection())
            clip->setText(text, QClipboard::Selection);
    }
}


// -------------------------------------------------------------------------------------------------
QString StationList::getCurrentEntriesForPrint() const
{
    QString ret;

    ret += "<table border=\"1\" width=\"100%\">";
    ret += "<tr>";
    ret += "<td width=\"10%\"><b>" + tr("Start") + "</b></td>";
    ret += "<td width=\"10%\"><b>" + tr("End") + "</b></td>";
    ret += "<td width=\"10%\"><b>" + tr("Language") + "</b></td>";
    ret += "<td width=\"5%\"><b>" + tr("ITU") + "</b></td>";
    ret += "<td width=\"20%\"><b>" + tr("Station") + "</b></td>";
    ret += "<td width=\"45%\"><b>" + tr("Frequencies") + "</b></td>";
    ret += "</tr>";

    Q3ListViewItemIterator it( const_cast<StationList*>(this) );

    while (it.current() ) {
        ret += dynamic_cast<StationListViewItem*>(*it)->getTransmission().toRichText();

        ++it;
    }

    ret += "</table>";

    return ret;
}


// -------------------------------------------------------------------------------------------------
QString StationList::getTextForExport() const
{
    static const QString line =
        "---------------------------------------------------------------------------------------\n";
    QString ret;
    QString temp;

    ret += line;
    temp.sprintf((Transmission::FORMAT_STRING + "\n").latin1(),
        static_cast<const char*>(tr("Start").utf8()),
        static_cast<const char*>(tr("End").utf8()),
        static_cast<const char*>(tr("Language").utf8()),
        static_cast<const char*>(tr("ITU").utf8()),
        static_cast<const char*>(tr("Station").utf8()),
        static_cast<const char*>(tr("Frequencies").utf8())
    );
    ret += temp;
    ret += line;

    Q3ListViewItemIterator it( const_cast<StationList*>(this) );

    while (it.current() ) {
        ret += dynamic_cast<StationListViewItem*>(*it)->getTransmission().toAlignedString();
        ret += "\n";
        ++it;
    }

    ret += line;

    return ret;
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
