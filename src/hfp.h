/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef HFP_H
#define HFP_H

#include <QtGui>

#include "itudialog.h"
#include "settings.h"
#include "centralwidget.h"

class Hfp : public QMainWindow
{
    Q_OBJECT

    public:
        Hfp();

        Settings& set();

    protected slots:
        void showAbout();
        void print();
        void exportText();
        void updateData();
        void showItuDialog();
        void autoUpdateToggled(bool state);

    protected:
        void closeEvent(QCloseEvent* evt);

    private:
        void initToolbar();
        void initMenubar();
        void initActions();
        void connectSignalsAndSlots();

    signals:
        void reloadData();

    private:
        struct Actions {
            QAction* quitAction;
            QAction* ituAction;
            QAction* aboutAction;
            QAction* aboutQtAction;
            QAction* printAction;
            QAction* exportAction;
            QAction* reloadAction;
            QAction* updateAction;
            QAction* autoUpdate;
        };

    private:
        Q_DISABLE_COPY(Hfp)
        CentralWidget       *m_centralWidget;
        Actions             m_actions;
        ItuDialog           *m_ituDialog;
        QTimer              *m_updateTimer;
};

#endif // HFP_H

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
