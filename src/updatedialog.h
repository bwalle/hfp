/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef UPDATEDIALOG_H
#define UPDATEDIALOG_H

#include <QtGui>
#include <Qt3Support>

class UpdateDialog : public QDialog
{
    Q_OBJECT

    public:
        UpdateDialog(QWidget* parent, const char* name = 0);

    protected slots:
        void updateURL(const QString& s);
        void doUpdate();
        void finishHandler(Q3NetworkOperation* op);
        void done(int r);

    protected:
        void updateLastUpdate();

    signals:
        void reloadDataNeeded();

    private:
        Q_DISABLE_COPY(UpdateDialog)
        QLineEdit*      m_lastUpdateEdit;
        QLineEdit*      m_urlEdit;
        Q3ProgressBar*   m_updateProgress;
        bool            m_finished;
        QString         m_errorString;
        Q3UrlOperator    m_urlOperator;
        QPushButton*    m_startButton;

};

#endif /* UPDATEDIALOG_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
