/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <memory>

#include <QtGui>
#include <Qt3Support>

#include "itudialog.h"
#include "aboutdialog.h"
#include "settings.h"
#include "worldclock.h"
#include "hfp.h"
#include "updatedialog.h"

using std::auto_ptr;

// -------------------------------------------------------------------------------------------------
Hfp* hfp;

// -------------------------------------------------------------------------------------------------
Hfp::Hfp()
    : QMainWindow(0, "qpamat main window")
    , m_updateTimer(new QTimer(this))
{
    // Title and Icon
    setIcon(QPixmap(":/images/hfp_32.png"));
    setCaption(tr("HFP - Frequency Schedule Program"));
    setUnifiedTitleAndToolBarOnMac(true);

    setIconSize(QSize(24, 24));

    // main widget in the center TODO
    m_centralWidget = new CentralWidget(this);
    setCentralWidget(m_centralWidget);

    // Initiamlization of menu
    initActions();
    initMenubar();
    initToolbar();

    // restore the layout
    restoreState(Settings::set().readByteArrayEntry("Main Window/layout"));
    if (Settings::set().readBoolEntry("Main Window/maximized"))
        showMaximized();
    else
        resize(
            Settings::set().readNumEntry("Main Window/width", int(qApp->desktop()->width() * 0.6) ),
            Settings::set().readNumEntry("Main Window/height", int(qApp->desktop()->height() / 2.0) )
        );

    // ITU dialog
    m_ituDialog = new ItuDialog(this, "ITU Dialog");

    connectSignalsAndSlots();

    QTimer::singleShot( 0, m_centralWidget, SLOT(loadTransmissionTable()) );
}

// -------------------------------------------------------------------------------------------------
void Hfp::showAbout()
{
    auto_ptr<AboutDialog> dlg(new AboutDialog(qApp->mainWidget(), "About Dialog"));

    dlg->exec();
}


// -------------------------------------------------------------------------------------------------
void Hfp::showItuDialog()
{
    m_ituDialog->show();
}

// -------------------------------------------------------------------------------------------------
void Hfp::autoUpdateToggled(bool state)
{
    if (state)
        m_updateTimer->start(60000);  // 1 minute
    else if (m_updateTimer->isActive())
        m_updateTimer->stop();
}

// -------------------------------------------------------------------------------------------------
void Hfp::initToolbar()
{
    // ----- Application ---------------------------------------------------------------------------
    QToolBar* applicationToolbar = new QToolBar(this, "applicationToolbar");
    applicationToolbar->setLabel(tr("Application"));
    addToolBar(Qt::TopToolBarArea, applicationToolbar);

    m_actions.quitAction->addTo(applicationToolbar);
    applicationToolbar->addSeparator();
    m_actions.exportAction->addTo(applicationToolbar);
    m_actions.printAction->addTo(applicationToolbar);
    applicationToolbar->addSeparator();
    m_actions.reloadAction->addTo(applicationToolbar);
    m_actions.updateAction->addTo(applicationToolbar);

    QToolBar* clockToolbar = new QToolBar(this, "clockToolbar");
    clockToolbar->setLabel(tr("Clock"));

	QWidget *spacerWidget = new QWidget(this);
	spacerWidget->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
	spacerWidget->setVisible(true);
    addToolBar(Qt::TopToolBarArea, clockToolbar);
#ifdef Q_WS_MAC
	clockToolbar->addWidget(spacerWidget);
#endif
    clockToolbar->addWidget(new WorldClock(clockToolbar));
}


// -------------------------------------------------------------------------------------------------
void Hfp::initMenubar()
{
     // ----- File ---------------------------------------------------------------------------------
     QMenu* fileMenu = new QMenu(this);
     menuBar()->insertItem(tr("&File"), fileMenu);

     m_actions.reloadAction->addTo(fileMenu);
     m_actions.updateAction->addTo(fileMenu);
     m_actions.autoUpdate->addTo(fileMenu);
     fileMenu->insertSeparator();
     m_actions.exportAction->addTo(fileMenu);
     m_actions.printAction->addTo(fileMenu);
     fileMenu->insertSeparator();
     m_actions.quitAction->addTo(fileMenu);

     // ----- Tools --------------------------------------------------------------------------------
     QMenu* toolsMenu = new QMenu(this);
     menuBar()->insertItem(tr("&Tools"), toolsMenu);

     m_actions.ituAction->addTo(toolsMenu);

     // ----- Help ---------------------------------------------------------------------------------
     menuBar()->insertSeparator();
     QMenu* helpMenu = new QMenu(this);
     menuBar()->insertItem(tr("&Help"), helpMenu);

     m_actions.aboutQtAction->addTo(helpMenu);
     m_actions.aboutAction->addTo(helpMenu);
}


// -------------------------------------------------------------------------------------------------
void Hfp::closeEvent(QCloseEvent* e)
{
    // write window layout
    QString layout;
    Settings::set().writeEntry("Main Window/layout", saveState());
    Settings::set().writeEntry("Main Window/width", size().width());
    Settings::set().writeEntry("Main Window/height", size().height());
    Settings::set().writeEntry("Main Window/maximized", isMaximized());
    Settings::set().writeEntry("General/AutoUpdate", m_actions.autoUpdate->isChecked());

    e->accept();
}


// -------------------------------------------------------------------------------------------------
void Hfp::exportText()
{
    QString fileName = QFileDialog::getSaveFileName(QDir::homeDirPath(), tr("Text files (*.txt)"),
        this, "ExportFileDialog", tr("HFP - Choose a file"));

    if (!fileName.isNull()) {
         QFile file(fileName);

         if (!file.open( QIODevice::WriteOnly | QIODevice::Text)) {
             QMessageBox::warning(this, "HFP",
                tr("Could not open the specified file for writing:\n\n%1").arg(file.errorString()),
                QMessageBox::Ok, QMessageBox::NoButton
             );

             return;
         }

        QTextStream stream(&file);
        stream << m_centralWidget->getStationList()->getTextForExport();
        file.close();
    }
}


// -------------------------------------------------------------------------------------------------
void Hfp::print()
{
    QPrinter printer( QPrinter::HighResolution );
    printer.setFullPage( TRUE );
    if ( printer.setup( this ) ) {
        QPainter p;
        if ( !p.begin( &printer ) )
            return;

        QFont serifFont("Times New Roman", 10);
        QFont smallSerifFont("Times New Roman", 8);

        p.setFont(smallSerifFont);

        qApp->setOverrideCursor( QCursor( Qt::WaitCursor ) );
        qApp->processEvents();

        Q3PaintDeviceMetrics metrics(p.device());
        int dpiy = metrics.logicalDpiY();
#define CON_MM(x)( int( ( (x)/25.4)*dpiy ) )
        int margin = CON_MM(20);
        QRect body( margin, margin, metrics.width() - 2 * margin,
            metrics.height() - 2 * margin - CON_MM(8) );
        Q3SimpleRichText richText(m_centralWidget->getStationList()->getCurrentEntriesForPrint(),
                serifFont, QString::null, 0,
                Q3MimeSourceFactory::defaultFactory(), body.height(), Qt::black, false );
        richText.setWidth( &p, body.width() );
        QRect view( body );
        QString programString = tr("HFP - Programme Schedule Program");
        QString dateString = QDate::currentDate().toString(Qt::ISODate) + " / " +
            QTime::currentTime().toString("hh:mm");

        for (int page = 1; ; ++page) {
            qApp->processEvents();
            QString pageS = tr("page") + " " + QString::number(page);

            richText.draw( &p, body.left(), body.top(), view, colorGroup() );
            view.moveBy(0, body.height() );
            p.translate(0 , -body.height() );

            int x_pos = int(( view.left() + p.fontMetrics().width(programString)
                + view.right() - p.fontMetrics().width( pageS ) ) / 2.0
                - p.fontMetrics().width( dateString ) / 2.0 );
            int y_pos = view.bottom() + p.fontMetrics().ascent() + CON_MM(8);
            p.drawText( view.left(), y_pos, programString);
            p.drawText( x_pos, y_pos, dateString );
            p.drawText( view.right() - p.fontMetrics().width(pageS), y_pos, pageS);
            if ( view.top() >= richText.height() )
            {
                break;
            }
            printer.newPage();
        }
#undef CONVERT_MM

        qApp->processEvents();
        qApp->restoreOverrideCursor();
    }
}


// -------------------------------------------------------------------------------------------------
void Hfp::connectSignalsAndSlots()
{
    // Actions
    connect(m_actions.quitAction,       SIGNAL(activated()),
            this,                       SLOT(close()));
    connect(m_actions.reloadAction,     SIGNAL(activated()),
            this,                       SIGNAL(reloadData()));
    connect(m_actions.ituAction,        SIGNAL(activated()),
            this,                       SLOT(showItuDialog()));
    connect(m_actions.aboutAction,      SIGNAL(activated()),
            this,                       SLOT(showAbout()));
    connect(m_actions.aboutQtAction,    SIGNAL(activated()),
            qApp,                       SLOT(aboutQt()));
    connect(m_actions.printAction,      SIGNAL(activated()),
            this,                       SLOT(print()));
    connect(m_actions.exportAction,     SIGNAL(activated()),
            this,                       SLOT(exportText()));
    connect(m_actions.updateAction,     SIGNAL(activated()),
            this,                       SLOT(updateData()));
    connect(m_actions.autoUpdate,       SIGNAL(toggled(bool)),
            this,                       SLOT(autoUpdateToggled(bool)));
    connect(m_updateTimer,              SIGNAL(timeout()),
            this,                       SIGNAL(reloadData()));
}


// -------------------------------------------------------------------------------------------------
void Hfp::updateData()
{
    std::auto_ptr<UpdateDialog> dlg(new UpdateDialog(this));
    connect(dlg.get(), SIGNAL(reloadDataNeeded()), SIGNAL(reloadData()));
    dlg->exec();
    disconnect(dlg.get());
}

// -------------------------------------------------------------------------------------------------
void Hfp::initActions()
{
    // ----- File ----------------------------------------------------------------------------------
    QIcon quitActionIcon(QPixmap(":/images/stock_exit_16.png"));
    quitActionIcon.addPixmap(QPixmap(":/images/stock_exit_24.png"));
    m_actions.quitAction = new QAction(quitActionIcon, tr("E&xit"), this);
    m_actions.quitAction->setShortcut(QKeySequence(Qt::CTRL|Qt::Key_Q));

    QIcon reloadActionIcon(QPixmap(":/images/stock_refresh_16.png"));
    reloadActionIcon.addPixmap(QPixmap(":/images/stock_refresh_24.png"));
    m_actions.reloadAction = new QAction(reloadActionIcon, tr("&Reload data"), this);
    m_actions.reloadAction->setShortcut(QKeySequence(Qt::CTRL|Qt::Key_R));

    QIcon exportActionIcon(QPixmap(":/images/stock_save_as_16.png"));
    exportActionIcon.addPixmap(QPixmap(":/images/stock_save_as_24.png"));
    m_actions.exportAction = new QAction(exportActionIcon, tr("&Export..."), this);
    m_actions.exportAction->setShortcut(QKeySequence(Qt::CTRL|Qt::Key_X));

    QIcon updateActionIcon(QPixmap(":/images/network_16.png"));
    updateActionIcon.addPixmap(QPixmap(":/images/network_24.png"));
    m_actions.updateAction = new QAction(updateActionIcon, tr("&Update data..."), this);

    m_actions.autoUpdate = new QAction(tr("&Automatic update"), this);
    m_actions.autoUpdate->setCheckable(true);
    m_actions.autoUpdate->setChecked(Settings::set().readBoolEntry("General/AutoUpdate"));
    autoUpdateToggled(m_actions.autoUpdate->isChecked());

    QIcon printActionIcon(QPixmap(":/images/stock_print_16.png"));
    printActionIcon.addPixmap(QPixmap(":/images/stock_print_24"));
    m_actions.printAction = new QAction(printActionIcon, tr("&Print..."), this);
    m_actions.updateAction->setShortcut(QKeySequence(Qt::CTRL|Qt::Key_P));

    // ----- Tools ---------------------------------------------------------------------------------
    m_actions.ituAction = new QAction(tr("&Country codes..."), this);
    m_actions.ituAction->setShortcut(QKeySequence(Qt::Key_F8));

    // ----- Help ----------------------------------------------------------------------------------
    m_actions.aboutAction = new QAction(tr("&About..."), this);
    m_actions.aboutQtAction = new QAction(tr("About &Qt..."), this);
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
