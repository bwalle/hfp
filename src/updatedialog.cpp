/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QtGui>
#include <Qt3Support>
#include <ziparchive/ZipArchive.h>
#include <ziparchive/ZipPlatform.h>

#include "settings.h"
#include "updatedialog.h"

UpdateDialog::UpdateDialog(QWidget* parent, const char* name)
    : QDialog(parent, name)
{
    setCaption(tr("HFP - Update"));

    QLabel* lastUpdateLabel = new QLabel(tr("Last update:"), this);
    m_lastUpdateEdit = new QLineEdit(this);
    m_lastUpdateEdit->setReadOnly(true);

    QLabel* urlLabel = new QLabel(tr("Address:"), this);
    m_urlEdit = new QLineEdit(this);
    m_urlEdit->setMinimumWidth(300);
    m_urlEdit->setText(Settings::set().readEntry("General/ADDX_URL"));

    QLabel* progressLabel = new QLabel(tr("Progress:"), this);
    m_updateProgress = new Q3ProgressBar(this);
    m_updateProgress->setPercentageVisible(false);

    // buttons
    Q3HBox* buttonBox = new Q3HBox(this);
    buttonBox->setSpacing(6);
    m_startButton = new QPushButton(tr("Start &update"), buttonBox);
    QPushButton* closeButton = new QPushButton(tr("&Close"), buttonBox);
    closeButton->setDefault(true);

    // layout elements
    Q3GridLayout* layout = new Q3GridLayout(this, 5, 2, 10, 6);
    layout->addWidget(lastUpdateLabel, 0, 0);
    layout->addWidget(m_lastUpdateEdit, 0, 1);
    layout->addWidget(urlLabel, 1, 0);
    layout->addWidget(m_urlEdit, 1, 1);
    layout->addWidget(progressLabel, 2, 0);
    layout->addWidget(m_updateProgress, 2, 1);
    layout->setRowSpacing(3, 20);
    layout->setRowStretch(3, 20);
    layout->addMultiCellWidget(buttonBox, 4, 4, 0, 1, Qt::AlignRight);

    connect(m_urlEdit, SIGNAL(textChanged(const QString&)), SLOT(updateURL(const QString&)));
    connect(closeButton, SIGNAL(clicked()), SLOT(accept()));
    connect(m_startButton, SIGNAL(clicked()), SLOT(doUpdate()));

    // network stuff
    q3InitNetworkProtocols();
    connect(&m_urlOperator, SIGNAL(dataTransferProgress(int, int, Q3NetworkOperation*)),
        m_updateProgress, SLOT(setProgress(int, int)));
    connect(&m_urlOperator, SIGNAL(finished(Q3NetworkOperation*)),
        SLOT(finishHandler(Q3NetworkOperation*)));

    updateLastUpdate();
}


// -------------------------------------------------------------------------------------------------
void UpdateDialog::updateURL(const QString& s)
{
    Settings::set().writeEntry("General/ADDX_URL", s);
}


// -------------------------------------------------------------------------------------------------
void UpdateDialog::done(int r)
{
    m_urlOperator.stop();

    QDialog::done(r);
}


// -------------------------------------------------------------------------------------------------
void UpdateDialog::finishHandler(Q3NetworkOperation* op)
{
    m_errorString = QString::null;

    if (op->state() == Q3NetworkProtocol::StFailed)
        m_errorString = op->protocolDetail();

    m_finished = true;
}


// -------------------------------------------------------------------------------------------------
void UpdateDialog::updateLastUpdate()
{
    QFile file(Settings::set().readEntry("General/Datadir") + "DEUTSCH.DAT");
    QFileInfo fileInfo(file);

    if (!fileInfo.exists()) {
        m_lastUpdateEdit->setText("Unknown");
        return;
    }

    m_lastUpdateEdit->setText(fileInfo.lastModified().toString("yyyy-MM-dd hh:mm"));

}


// -------------------------------------------------------------------------------------------------
void UpdateDialog::doUpdate()
{
    m_startButton->setEnabled(false);

    QString dir = Settings::set().readEntry("General/Datadir");
    Q3Url url(Settings::set().readEntry("General/ADDX_URL"));
    if (!QDir(dir).exists()) {
        QDir home = QDir::homeDirPath();

        if (!home.mkdir(dir, true)) {
            QMessageBox::warning(this, "HFP",
                tr("Failed to create the directory %1. Check permissions.").arg(dir),
                QMessageBox::Ok, QMessageBox::NoButton
            );
            m_startButton->setEnabled(true);
            return;
        }
    }

    m_finished = false;

    m_urlOperator.copy(url, "file:" + dir  );

    while (!m_finished)
        qApp->processEvents();

    if (!m_errorString.isNull()) {
        QMessageBox::warning(this, "HFP",
            tr("Failed to download the data file:\n\n%1").arg(m_errorString),
            QMessageBox::Ok, QMessageBox::NoButton
        );
    }

    // XXX: unclean solution
    // wait until the file is on the disk which takes some slot processing
    // (at least on Windows)
    QString fullFileName = dir + "/" + url.fileName();
    QTime start = QTime::currentTime();
    while (!QFile::exists(fullFileName) && start.msecsTo(QTime::currentTime()) < 5000)
        qApp->processEvents();

    if (!QFile::exists(fullFileName)) {
        QMessageBox::warning(this, "HFP",
            tr("Failed to download the data file:\n\n%1").arg(m_errorString),
            QMessageBox::Ok, QMessageBox::NoButton
        );
    }

    CZipArchive zip;
    try {
        zip.Open(fullFileName);
        for (ZIP_INDEX_TYPE index = 0; index < zip.GetCount(); index++) {
            zip.ExtractFile(index, dir);

            // make the GUI more responsive
            qApp->processEvents();
        }
    } catch (...) {
        zip.Close(CZipArchive::afAfterException);
        QMessageBox::warning(this, "HFP",
            tr("Failed to extract file %1.").arg(fullFileName),
            QMessageBox::Ok, QMessageBox::NoButton
        );
        m_startButton->setEnabled(true);
        return;
    }

    zip.Close();
    m_startButton->setEnabled(true);
    updateLastUpdate();

    if (!QFile::remove(fullFileName)) {
        QMessageBox::warning(this, "HFP",
            tr("Failed to extract file %1.").arg(fullFileName),
            QMessageBox::Ok, QMessageBox::NoButton
        );
    }

    emit reloadDataNeeded();
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
