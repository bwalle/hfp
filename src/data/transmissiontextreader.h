/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef TRANSMISSIONTEXTREADER_H
#define TRANSMISSIONTEXTREADER_H

#include <stdexcept>

#include <QtCore>

#include "transmissiondatareader.h"

class TransmissionTextReader : public TransmissionDataReader
{
    public:
        TransmissionTextReader() {}

    public:
        void addTransmissions(const QString& fileName, TransmissionTable& table)
        throw (std::invalid_argument, FormatException);

        // should check according to the file extension, not content
        bool acceptsFile(const QString& fileName);

    protected:
        Transmission convertToTransmission(const QString& line, Transmission::Language lang,
                                           const QString& fileName)
        throw (std::invalid_argument, FormatException);

    private:
        Q_DISABLE_COPY(TransmissionTextReader)
};

#endif /* TRANSMISSIONTEXTREADER_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
