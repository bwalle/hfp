/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QtCore>

#include "transmission.h"
#include "transmissionfilter.h"

// -------------------------------------------------------------------------------------------------
TransmissionFilter::TransmissionFilter()
    : m_languages(0xffff), m_timeFilterType(TAll)
{
}

// -------------------------------------------------------------------------------------------------
bool TransmissionFilter::accept(const Transmission& transmission) const
{
    // check the language
    if (!(transmission.getLanguage() & m_languages))
        return false;

    Time time = m_timeValue;

    // now check the time
    switch (m_timeFilterType) {
        case TBeginsAt:
            if (uint(QABS(transmission.getBeginTime().secsTo(m_timeValue))) > m_tolerance*60)
                return false;
            break;

        case TEndsAt:
            if (uint(QABS(transmission.getEndTime().secsTo(m_timeValue))) > m_tolerance*60)
                return false;
            break;

        case TIsActiveNow:
            time = Time::currentTime(Qt::UTC);

            if ( ( transmission.getBeginTime() > time.addSecs(m_tolerance*60)
                    || transmission.getEndTime() <= time )
                && ( transmission.getBeginTime().addSecs(86400) > time.addSecs(m_tolerance*60)
                    || transmission.getEndTime().addSecs(86400) <= time )
                )
                return false;
            break;

        case TIsActiveAt:
            time = m_timeValue;

            if ( ( transmission.getBeginTime() > time.addSecs(m_tolerance*60)
                    || transmission.getEndTime() <= time )
                && ( transmission.getBeginTime().addSecs(86400) > time.addSecs(m_tolerance*60)
                    || transmission.getEndTime().addSecs(86400) <= time )
                )
                return false;
            break;

        case TAll:
        default:
            /* do nothing */
            break;
    }

    // station regexp
    if (m_stationRegexp.isValid()) {
        int result = m_stationRegexp.indexIn(transmission.getStation());
        if ((!m_stationNegate && result == -1) ||
                (m_stationNegate && result != -1))
            return false;
    }

    // frequency regexp
    if (m_frequencyRegexp.isValid()) {
        int result = m_frequencyRegexp.indexIn(transmission.getFrequencies());
        if ((!m_frequencyNegate && result == -1) ||
                (m_frequencyNegate && result != -1))
            return false;
    }

    // country regexp
    if (m_countryRegexp.isValid()) {
        int result = m_countryRegexp.indexIn(transmission.getCountry());
        if ((!m_countryNegate && result == -1) ||
                (m_countryNegate && result != -1))
            return false;
    }

    return true;
}


// -------------------------------------------------------------------------------------------------
void TransmissionFilter::setLanguages(int languages)
{
    m_languages = languages;
}


// -------------------------------------------------------------------------------------------------
void TransmissionFilter::setTime(TimeFilterType type, const Time& time)
{
    m_timeFilterType = type;
    m_timeValue = time;
}


// -------------------------------------------------------------------------------------------------
void TransmissionFilter::setStationRegexp(const QRegExp& rx, bool negate)
{
    m_stationRegexp = rx;
    m_stationNegate = negate;
}


// -------------------------------------------------------------------------------------------------
void TransmissionFilter::setFrequencyRegexp(const QRegExp& rx, bool negate)
{
    m_frequencyRegexp = rx;
    m_frequencyNegate = negate;
}


// -------------------------------------------------------------------------------------------------
void TransmissionFilter::setCountryRegexp(const QRegExp& rx, bool negate)
{
    m_countryRegexp = rx;
    m_countryNegate = negate;
}


// -------------------------------------------------------------------------------------------------
void TransmissionFilter::setTolerance(uint tolerance)
{
    m_tolerance = tolerance;
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
