/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef TRANSMISSIONDATAREADER_H
#define TRANSMISSIONDATAREADER_H

#include <QObject>

#include "formatexception.h"
#include "transmissiontable.h"

class TransmissionDataReader
{
    public:
        TransmissionDataReader() {}
        virtual ~TransmissionDataReader() {}

    public:
        virtual void addTransmissions(const QString& fileName, TransmissionTable& table)
        throw (std::invalid_argument, FormatException) = 0;

        // should check according to the file extension, not content
        virtual bool acceptsFile(const QString& fileName) = 0;

    private:
        Q_DISABLE_COPY(TransmissionDataReader)
};

#endif /* TRANSMISSIONDATAREADER_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
