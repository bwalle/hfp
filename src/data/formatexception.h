/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef FORMATEXCEPTION_H
#define FORMATEXCEPTION_H

#include <stdexcept>

#include <QtCore>

class FormatException : public std::runtime_error
{
    public:
        FormatException(const QString& error)
            : std::runtime_error((const char *)error.utf8()) {}

        ~FormatException() throw () {};
};

#endif /* FORMATEXCEPTION_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
