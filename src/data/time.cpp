/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QtCore>

#include "time.h"

// -------------------------------------------------------------------------------------------------
Time::Time()
    : m_time(0)
{}


// -------------------------------------------------------------------------------------------------
Time::Time(uint hour, uint min)
    : m_time(hour * 60 + min)
{}


// -------------------------------------------------------------------------------------------------
Time::Time(const QTime& time)
    : m_time(time.hour() * 60 + time.minute())
{}


// -------------------------------------------------------------------------------------------------
bool Time::operator<(const Time& compare)
{
    return m_time < compare.m_time;
}


// -------------------------------------------------------------------------------------------------
bool Time::operator<=(const Time& compare)
{
    return m_time <= compare.m_time;
}

// -------------------------------------------------------------------------------------------------
bool Time::operator>(const Time& compare)
{
    return m_time > compare.m_time;
}


// -------------------------------------------------------------------------------------------------
bool Time::operator>=(const Time& compare)
{
    return m_time >= compare.m_time;
}


// -------------------------------------------------------------------------------------------------
bool Time::operator==(const Time& compare)
{
    return m_time == compare.m_time;
}


// -------------------------------------------------------------------------------------------------
bool Time::operator!=(const Time& compare)
{
    return m_time != compare.m_time;
}


// -------------------------------------------------------------------------------------------------
int Time::compare(const Time& t) const
{
    return m_time - t.m_time;
}

// -------------------------------------------------------------------------------------------------
QString Time::toString() const
{
    QString ret;

    ret.sprintf("%02d:%02d", m_time / 60, m_time % 60);

    return ret;
}


// -------------------------------------------------------------------------------------------------
int Time::secsTo(const Time& t) const
{
    return (t.m_time - m_time) * 60;
}


// -------------------------------------------------------------------------------------------------
Time Time::currentTime(Qt::TimeSpec ts)
{
    return static_cast<Time>(QTime::currentTime(ts));
}


// -------------------------------------------------------------------------------------------------
Time Time::addSecs(int secs) const
{
    Time copy = *this;

    copy.m_time += secs/60;

    return copy;
}


// -------------------------------------------------------------------------------------------------
Time Time::fromString(const QString& s)
{
    return Time( s.left(2).toInt(), s.right(2).toInt());
}


// -------------------------------------------------------------------------------------------------
int Time::getHour() const
{
    return m_time / 60;
}


// -------------------------------------------------------------------------------------------------
int Time::getMinute() const
{
    return m_time % 60;
}


// -------------------------------------------------------------------------------------------------
void Time::setTime(uint hour, uint min)
{
    m_time = hour * 60 + min;
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
