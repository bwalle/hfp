/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <stdexcept>

#include <QtGui>
#include <Qt3Support>

#include "transmissiontextreader.h"
#include "transmission.h"
#include "transmissionreader.h"
#include "charsetconv.h"
#include "time.h"

using std::invalid_argument;

// -------------------------------------------------------------------------------------------------
void TransmissionTextReader::addTransmissions(const QString& fileName, TransmissionTable& table)
    throw (invalid_argument, FormatException)
{
    QFile file(fileName);

    if (!file.open(QIODevice::ReadOnly))
        throw invalid_argument(
            QObject::tr("The file %1 does not exist.").arg(fileName).latin1()
        );

    QFileInfo fi(fileName);
    Transmission::Language lang = TransmissionReader::fileNameToLanguage(fi.baseName());

    Q3TextStream stream( &file );
    QString line;
    while ( !stream.atEnd() ) {
        line = stream.readLine();

        if (line.stripWhiteSpace().length() != 0)
            table.addTransmission(convertToTransmission(line, lang, fileName));
    }

    file.close();

    file.close();
}


// -------------------------------------------------------------------------------------------------
Transmission TransmissionTextReader::convertToTransmission(const QString& line,
                                                           Transmission::Language lang,
                                                           const QString& fileName)
    throw (std::invalid_argument, FormatException)
{
    Transmission newTransmission;

    QStringList elements;
    elements = QStringList::split(QRegExp("\t+"), line, false);

    if (elements.size() != 5)
        throw FormatException(fileName + ":\nThere must be 5 entries in a line.");

    newTransmission.setBeginTime( Time::fromString(elements[0]) );
    newTransmission.setEndTime( Time::fromString(elements[1]) );
    newTransmission.setCountry(elements[2]);
    newTransmission.setStation(elements[3]);
    newTransmission.setFrequencies(elements[4]);
    newTransmission.setLanguage(lang);


    return newTransmission;
}


// -------------------------------------------------------------------------------------------------
bool TransmissionTextReader::acceptsFile(const QString& fileName)
{
    return fileName.lower().endsWith(".txt");
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
