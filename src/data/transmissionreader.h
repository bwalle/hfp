/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef TRANSMISSIONREADER_H
#define TRANSMISSIONREADER_H

#include <QtCore>

#include "formatexception.h"
#include "transmission.h"
#include "transmissiontable.h"
#include "transmissiondatareader.h"

class TransmissionReader
{
    public:
        TransmissionReader();

        void addTransmissions(const QString& fileName, TransmissionTable& table)
        throw (std::invalid_argument, FormatException);

        void addAllTransmissions(const QString& dirName, TransmissionTable& table)
        throw (std::invalid_argument, FormatException);

        static Transmission::Language fileNameToLanguage(const QString& fileName);

    private:
        Q3PtrVector<TransmissionDataReader> m_readers;
};

#endif /* TRANSMISSIONREADER_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
