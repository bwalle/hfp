/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef TRANSMISSION_H
#define TRANSMISSION_H

#include <stdexcept>

#include <QtCore>

#include "global.h"
#include "time.h"

class Transmission
{
    public:
        enum Language {
            LGerman     = 1,
            LEnglish    = 2,
            LFrench     = 4,
            LSpanish    = 8,
            LTurkish    = 16,
            LRussian    = 32,
            LUnknown    = 64
        };

    public:

        Time getBeginTime() const;
        QString getBeginTimeAsString() const;
        Time getEndTime() const;
        QString getEndTimeAsString() const;
        QString getCountry() const;
        QString getStation() const;
        QString getFrequencies() const;
        QString getLanguageAsString() const;
        Language getLanguage() const;

        void setBeginTime(const Time& time);
        void setEndTime(const Time& time);
        void setCountry(const QString& country);
        void setStation(const QString& station);
        void setFrequencies(const QString& frequencies);
        void setLanguage(Language lang);

        QString toRichText() const;
        QString toAlignedString() const;

    public:
        static const QString FORMAT_STRING;

    private:
        Time m_beginTime;
        QString m_beginTimeAsString;
        Time m_endTime;
        QString m_endTimeAsString;
        QString m_country;
        QString m_station;
        QString m_frequencies;
        Language m_language;

};

#endif /* TRANSMISSION_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
