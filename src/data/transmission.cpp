/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QtCore>

#include "transmission.h"
#include "charsetconv.h"
#include "time.h"

// -------------------------------------------------------------------------------------------------
const QString Transmission::FORMAT_STRING =
    "%-5.5s  %-5.5s  %-10.10s %-5.5s %-20.20s %-30.30s";

// -------------------------------------------------------------------------------------------------
Time Transmission::getBeginTime() const
{
    return m_beginTime;
}


// -------------------------------------------------------------------------------------------------
QString Transmission::getBeginTimeAsString() const
{
    return m_beginTimeAsString;
}


// -------------------------------------------------------------------------------------------------
Time Transmission::getEndTime() const
{

    return m_endTime;
}


// -------------------------------------------------------------------------------------------------
QString Transmission::getEndTimeAsString() const
{
    return m_endTimeAsString;
}


// -------------------------------------------------------------------------------------------------
QString Transmission::getCountry() const
{

    return m_country;
}


// -------------------------------------------------------------------------------------------------
QString Transmission::getFrequencies() const
{
    return m_frequencies;
}


// -------------------------------------------------------------------------------------------------
QString Transmission::getStation() const
{
    return m_station;
}


// -------------------------------------------------------------------------------------------------
Transmission::Language Transmission::getLanguage() const
{
    return m_language;
}


// -------------------------------------------------------------------------------------------------
void Transmission::setBeginTime(const Time& time)
{
    m_beginTime = time;
    m_beginTimeAsString = time.toString();
}


// -------------------------------------------------------------------------------------------------
void Transmission::setEndTime(const Time& time)
{
    m_endTime = time;
    m_endTimeAsString = time.toString();
}


// -------------------------------------------------------------------------------------------------
void Transmission::setCountry(const QString& country)
{
    m_country = country;
}


// -------------------------------------------------------------------------------------------------
void Transmission::setStation(const QString& station)
{
    m_station = station;
}


// -------------------------------------------------------------------------------------------------
void Transmission::setFrequencies(const QString& frequencies)
{
    m_frequencies = frequencies;
}


// -------------------------------------------------------------------------------------------------
void Transmission::setLanguage(Language lang)
{
    m_language = lang;
}


// -------------------------------------------------------------------------------------------------
QString Transmission::getLanguageAsString() const
{
    switch (m_language) {
        case LGerman:
            return QObject::tr("German");
        case LEnglish:
            return QObject::tr("English");
        case LFrench:
            return QObject::tr("French");
        case LSpanish:
            return QObject::tr("Spanish");
        case LTurkish:
            return QObject::tr("Turkish");
        case LRussian:
            return QObject::tr("Russian");
        default:
            return QObject::tr("Unknown");
    }
}


// -------------------------------------------------------------------------------------------------
QString Transmission::toRichText() const
{
    QString ret;

    ret += "<tr>";
    ret += "<td>" + getBeginTimeAsString() + "</td>";
    ret += "<td>" + getEndTimeAsString() + "</td>";
    ret += "<td>" + getLanguageAsString() + "</td>";
    ret += "<td>" + m_country + "</td>";
    ret += "<td>" + m_station + "</td>";
    ret += "<td>" + m_frequencies + "</td>";
    ret += "</tr>";

    return ret;
}


// -------------------------------------------------------------------------------------------------
QString Transmission::toAlignedString() const
{
    QString ret;

    ret.sprintf(FORMAT_STRING.latin1(),
        static_cast<const char*>(getBeginTimeAsString().utf8()),
        static_cast<const char*>(getEndTimeAsString().utf8()),
        static_cast<const char*>(getLanguageAsString().utf8()),
        static_cast<const char*>(m_country.utf8()),
        static_cast<const char*>(m_station.utf8()),
        static_cast<const char*>(m_frequencies.utf8()));

    return ret.stripWhiteSpace();
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
