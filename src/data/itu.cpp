/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QtGui>
#include <Qt3Support>

#include "global.h"
#include "itu.h"

StringMap ITU::s_map = ITU::initStringMap();

static const char* ITU_MAP[] = {
    "EGY",      QT_TR_NOOP("Egypt"),
    "GNE",      QT_TR_NOOP("Equatorial Guinea"),
    "ETH",      QT_TR_NOOP("Ethiopia"),
    "AFG",      QT_TR_NOOP("Afghanistan"),
    "ALS",      QT_TR_NOOP("Alaska"),
    "ALB",      QT_TR_NOOP("Albania"),
    "ALG",      QT_TR_NOOP("Algeria"),
    "AND",      QT_TR_NOOP("Andorra"),
    "AGL",      QT_TR_NOOP("Angola"),
    "AIA",      QT_TR_NOOP("Anguilla"),
    "ATA",      QT_TR_NOOP("Antarctica"),
    "ATG",      QT_TR_NOOP("Antigua"),
    "ARG",      QT_TR_NOOP("Argentina"),
    "ARM",      QT_TR_NOOP("Armenia"),
    "ASC",      QT_TR_NOOP("Ascension"),
    "AZE",      QT_TR_NOOP("Azerbaijan"),
    "AUS",      QT_TR_NOOP("Australia"),
    "AZR",      QT_TR_NOOP("Azores"),
    "BAH",      QT_TR_NOOP("Bahamas"),
    "BHR",      QT_TR_NOOP("Bahrain"),
    "BGD",      QT_TR_NOOP("Bangladesh"),
    "BRB",      QT_TR_NOOP("Barbados"),
    "BLR",      QT_TR_NOOP("Belarus"),
    "BEL",      QT_TR_NOOP("Belgium"),
    "BLZ",      QT_TR_NOOP("Belize"),
    "BEN",      QT_TR_NOOP("Benin"),
    "BTN",      QT_TR_NOOP("Bhutan"),
    "BOL",      QT_TR_NOOP("Bolivia"),
    "BIH",      QT_TR_NOOP("Bosnia and Herzegovina"),
    "BOT",      QT_TR_NOOP("Botswana"),
    "B",        QT_TR_NOOP("Brasilia"),
    "BRU",      QT_TR_NOOP("Brunei"),
    "BUL",      QT_TR_NOOP("Bulgaria"),
    "BFA",      QT_TR_NOOP("Burkina Faso"),
    "BDI",      QT_TR_NOOP("Burundi"),
    "CHL",      QT_TR_NOOP("Chile"),
    "CHN",      QT_TR_NOOP("China"),
    "CTR",      QT_TR_NOOP("Costa Rica"),
    "CTI",      QT_TR_NOOP("Cote d'Ivoire"),
    "DNK",      QT_TR_NOOP("Denmark"),
    "D",        QT_TR_NOOP("Germany"),
    "DGA",      QT_TR_NOOP("Diego Garc�a"),
    "DMA",      QT_TR_NOOP("Dominica"),
    "DOM",      QT_TR_NOOP("Dominican Republic"),
    "DJI",      QT_TR_NOOP("Dschibuti"),
    "EQA",      QT_TR_NOOP("Ecuador"),
    "SLV",      QT_TR_NOOP("El Salvador"),
    "ERI",      QT_TR_NOOP("Eritrea"),
    "EST",      QT_TR_NOOP("Estonia"),
    "FAR",      QT_TR_NOOP("Faroe islands"),
    "FJI",      QT_TR_NOOP("Fiji"),
    "FIN",      QT_TR_NOOP("Finland"),
    "F",        QT_TR_NOOP("France"),
    "GAB",      QT_TR_NOOP("Gabun"),
    "GMB",      QT_TR_NOOP("Gambia"),
    "GEO",      QT_TR_NOOP("Georgia"),
    "GHA",      QT_TR_NOOP("Ghana"),
    "GIB",      QT_TR_NOOP("Gibraltar"),
    "GRD",      QT_TR_NOOP("Grenada"),
    "GRC",      QT_TR_NOOP("Greece"),
    "GRL",      QT_TR_NOOP("Greenland"),
    "G",        QT_TR_NOOP("Great Britain"),
    "GDL",      QT_TR_NOOP("Guadeloupe"),
    "GUM",      QT_TR_NOOP("Guam"),
    "GTM",      QT_TR_NOOP("Guatemala"),
    "GUF",      QT_TR_NOOP("French Guiana"),
    "GUI",      QT_TR_NOOP("Guinea"),
    "GUY",      QT_TR_NOOP("Guyana"),
    "HWA",      QT_TR_NOOP("Hawaii"),
    "HND",      QT_TR_NOOP("Honduras"),
    "IND",      QT_TR_NOOP("India"),
    "INS",      QT_TR_NOOP("Indonesia"),
    "IRQ",      QT_TR_NOOP("Iraq"),
    "IRN",      QT_TR_NOOP("Iran"),
    "IRL",      QT_TR_NOOP("Ireland"),
    "ISL",      QT_TR_NOOP("Iceland"),
    "ISR",      QT_TR_NOOP("Israel"),
    "I",        QT_TR_NOOP("Italy"),
    "JMC",      QT_TR_NOOP("Jamaica"),
    "J",        QT_TR_NOOP("Japan"),
    "YEM",      QT_TR_NOOP("Yemen"),
    "JOR",      QT_TR_NOOP("Jordan"),
    "CYM",      QT_TR_NOOP("Cayman Islands"),
    "CBG",      QT_TR_NOOP("Cambodia"),
    "CME",      QT_TR_NOOP("Cameroon"),
    "CAN",      QT_TR_NOOP("Canada"),
    "CNR",      QT_TR_NOOP("Canary Islands"),
    "KAZ",      QT_TR_NOOP("Kazakhstan"),
    "QAT",      QT_TR_NOOP("Qatar"),
    "KEN",      QT_TR_NOOP("Kenyia"),
    "KGZ",      QT_TR_NOOP("Kyrgyzstan"),
    "KIR",      QT_TR_NOOP("Kiribati"),
    "CLM",      QT_TR_NOOP("Colombia"),
    "COM",      QT_TR_NOOP("Comoros"),
    "ZAI",      QT_TR_NOOP("Democratic Republic of the Congo"),
    "COG",      QT_TR_NOOP("Republic of the Congo"),
    "KRE",      QT_TR_NOOP("North Korea"),
    "KOR",      QT_TR_NOOP("South Korea"),
    "HRV",      QT_TR_NOOP("Croatia"),
    "CUB",      QT_TR_NOOP("Cuba"),
    "KWT",      QT_TR_NOOP("Kuwait"),
    "LAO",      QT_TR_NOOP("Laos"),
    "LSO",      QT_TR_NOOP("Lesotho"),
    "LVA",      QT_TR_NOOP("Latvia"),
    "LBN",      QT_TR_NOOP("Libanon"),
    "LBR",      QT_TR_NOOP("Liberia"),
    "LBY",      QT_TR_NOOP("Libya"),
    "LTU",      QT_TR_NOOP("Lithuania"),
    "LUX",      QT_TR_NOOP("Luxembourg"),
    "MDG",      QT_TR_NOOP("Madagascar"),
    "MDR",      QT_TR_NOOP("Madeira"),
    "MWI",      QT_TR_NOOP("Malawi"),
    "MLA",      QT_TR_NOOP("Malaysia"),
    "MLD",      QT_TR_NOOP("Maldives"),
    "MLI",      QT_TR_NOOP("Mali"),
    "MLT",      QT_TR_NOOP("Malta"),
    "MRA",      QT_TR_NOOP("Northern Mariana Islands"),
    "MRC",      QT_TR_NOOP("Morocco"),
    "MRT",      QT_TR_NOOP("Martinique"),
    "MTN",      QT_TR_NOOP("Mauritania"),
    "MAU",      QT_TR_NOOP("Mauritius"),
    "MYT",      QT_TR_NOOP("Mayotte"),
    "MKD",      QT_TR_NOOP("Macedonia"),
    "MEX",      QT_TR_NOOP("Mexico"),
    "MDA",      QT_TR_NOOP("Moldova"),
    "MCO",      QT_TR_NOOP("Monaco"),
    "MNG",      QT_TR_NOOP("Mongolia"),
    "MOZ",      QT_TR_NOOP("Mozambique"),
    "BRM",      QT_TR_NOOP("Myanmar (Birma)"),
    "NMB",      QT_TR_NOOP("Namibia"),
    "NPL",      QT_TR_NOOP("Nepal"),
    "NCL",      QT_TR_NOOP("New Caledonia"),
    "NZL",      QT_TR_NOOP("New Zealand"),
    "NCG",      QT_TR_NOOP("Nicaragua"),
    "ATN",      QT_TR_NOOP("Netherlands Antilles"),
    "HOL",      QT_TR_NOOP("Netherlands"),
    "NGR",      QT_TR_NOOP("Niger"),
    "NIG",      QT_TR_NOOP("Nigeria"),
    "NOR",      QT_TR_NOOP("Norway"),
    "AUT",      QT_TR_NOOP("Austria"),
    "OMA",      QT_TR_NOOP("Oman"),
    "PAK",      QT_TR_NOOP("Pakistan"),
    "PAL",      QT_TR_NOOP("Palau"),
    "PNG",      QT_TR_NOOP("Papua New Guinea"),
    "PRG",      QT_TR_NOOP("Paraguay"),
    "PRU",      QT_TR_NOOP("Peru"),
    "PHL",      QT_TR_NOOP("Philippines"),
    "POL",      QT_TR_NOOP("Poland"),
    "OCE",      QT_TR_NOOP("French Polynesia"),
    "POR",      QT_TR_NOOP("Portugal"),
    "PRT",      QT_TR_NOOP("Puerto Rico"),
    "REU",      QT_TR_NOOP("R�union"),
    "RRW",      QT_TR_NOOP("Rwanda"),
    "ROU",      QT_TR_NOOP("Romania"),
    "RUS",      QT_TR_NOOP("Russia"),
    "SLM",      QT_TR_NOOP("Solomon Islands"),
    "ZMB",      QT_TR_NOOP("Zambia"),
    "STP",      QT_TR_NOOP("Sao Tome and Principe"),
    "ARS",      QT_TR_NOOP("Saudi Arabia"),
    "S",        QT_TR_NOOP("Sweden"),
    "SUI",      QT_TR_NOOP("Switzerland"),
    "SEN",      QT_TR_NOOP("Senegal"),
    "SCG",      QT_TR_NOOP("Serbia and Montenegro"),
    "SEY",      QT_TR_NOOP("Seychelles"),
    "SRL",      QT_TR_NOOP("Sierra Leone"),
    "ZWE",      QT_TR_NOOP("Zimbabwe"),
    "SNG",      QT_TR_NOOP("Singapore"),
    "SVK",      QT_TR_NOOP("Slovakia"),
    "SVN",      QT_TR_NOOP("Slovenia"),
    "SOM",      QT_TR_NOOP("Somalia"),
    "E",        QT_TR_NOOP("Spain"),
    "CLN",      QT_TR_NOOP("Sri Lanka"),
    "SHN",      QT_TR_NOOP("St. Helena"),
    "SCN",      QT_TR_NOOP("Saint Kitts and Nevis"),
    "SPM",      QT_TR_NOOP("St. Pierre and Miquelon"),
    "SDN",      QT_TR_NOOP("Sudan"),
    "AFS",      QT_TR_NOOP("South Africa"),
    "SUR",      QT_TR_NOOP("Suriname"),
    "SWZ",      QT_TR_NOOP("Swaziland"),
    "SYR",      QT_TR_NOOP("Syria"),
    "TJK",      QT_TR_NOOP("Tajikistan"),
    "TWN",      QT_TR_NOOP("Taiwan"),
    "TZA",      QT_TR_NOOP("United Republic of Tanzania"),
    "THA",      QT_TR_NOOP("Thailand"),
    "TGO",      QT_TR_NOOP("Togo"),
    "TON",      QT_TR_NOOP("Tonga"),
    "TCD",      QT_TR_NOOP("Chad"),
    "CZE",      QT_TR_NOOP("Czech Republic"),
    "TUR",      QT_TR_NOOP("Turkey"),
    "TUN",      QT_TR_NOOP("Tunisia"),
    "TCA",      QT_TR_NOOP("Turks and Caicos Islands"),
    "TKM",      QT_TR_NOOP("Turkmenistan"),
    "UGA",      QT_TR_NOOP("Uganda"),
    "UKR",      QT_TR_NOOP("Ukraine"),
    "ONU",      QT_TR_NOOP("United Nations"),
    "HNG",      QT_TR_NOOP("Hungary"),
    "URG",      QT_TR_NOOP("Uruguay"),
    "USA",      QT_TR_NOOP("United States of America (USA)"),
    "UZB",      QT_TR_NOOP("Uzbekistan"),
    "VUT",      QT_TR_NOOP("Vanuatu"),
    "CVA",      QT_TR_NOOP("Vatican"),
    "VEN",      QT_TR_NOOP("Venezuela"),
    "UAE",      QT_TR_NOOP("United Arab Emirates"),
    "VTN",      QT_TR_NOOP("Vietnam"),
    "WAL",      QT_TR_NOOP("Wallis and Futuna Islands"),
    "CAF",      QT_TR_NOOP("Central African Republic"),
    "CYP",      QT_TR_NOOP("Cyprus")
};


// -------------------------------------------------------------------------------------------------
StringMap ITU::initStringMap()
{
    StringMap temp;

    for (uint i = 0; i < sizeof(ITU_MAP)/sizeof(char*); i += 2)
        temp[ITU_MAP[i]] = ITU_MAP[i+1];

    return temp;
}


// -------------------------------------------------------------------------------------------------
StringList ITU::getITUs()
{
    return s_map.keys();
}


// -------------------------------------------------------------------------------------------------
QString ITU::getCountry(const QString& itu)
{
    return qApp->translate("", s_map[itu]);
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
