/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef TIME_H
#define TIME_H

#include <QtCore>

class Time
{
    public:
        Time();
        Time(uint hour, uint min);
        explicit Time(const QTime& time);

    public:
        int secsTo(const Time& t) const;
        QString toString() const;
        Time addSecs(int secs) const;

        int getHour() const;
        int getMinute() const;
        void setTime(uint hour, uint min);

        int compare(const Time& t) const;

        bool operator<(const Time& compare);
        bool operator<=(const Time& compare);
        bool operator>(const Time& compare);
        bool operator>=(const Time& compare);
        bool operator==(const Time& compare);
        bool operator!=(const Time& compare);

    public:
        static Time currentTime(Qt::TimeSpec t);
        static Time fromString(const QString& s);

    private:
        int m_time;
};

#endif /* TIME_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
