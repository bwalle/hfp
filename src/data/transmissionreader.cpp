/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include "transmissionreader.h"
#include "transmission.h"

#include <stdexcept>

#include <QtGui>
#include <Qt3Support>

#include "transmissiondatareader.h"
#include "transmissionaddxreader.h"
#include "transmissiontextreader.h"

using std::invalid_argument;


// -------------------------------------------------------------------------------------------------
TransmissionReader::TransmissionReader()
{
    m_readers.setAutoDelete(true);

    m_readers.resize(2);
    m_readers.insert(0, new TransmissionADDXReader());
    m_readers.insert(1, new TransmissionTextReader());
}

// -------------------------------------------------------------------------------------------------
Transmission::Language TransmissionReader::fileNameToLanguage(const QString& fileName)
{
    if (fileName.lower().startsWith("deu"))
        return Transmission::LGerman;
    else if (fileName.lower().startsWith("eng"))
        return Transmission::LEnglish;
    else if (fileName.lower().startsWith("fra"))
        return Transmission::LFrench;
    else if (fileName.lower().startsWith("spa"))
        return Transmission::LSpanish;
    else if (fileName.lower().startsWith("t"))
        return Transmission::LTurkish;
    else if (fileName.lower().startsWith("rus"))
        return Transmission::LRussian;
    else
        return Transmission::LUnknown;
}


// -------------------------------------------------------------------------------------------------
void TransmissionReader::addAllTransmissions(const QString& dirName, TransmissionTable& table)
    throw (std::invalid_argument, FormatException)
{
    QDir d(dirName);
    if (!d.exists())
        throw invalid_argument((const char *)QObject::tr("Directory %1 does not exist.").arg(dirName).utf8() );

    d.setFilter(QDir::Files);

    QList<QFileInfo> list = d.entryInfoList();

    for (QList<QFileInfo>::const_iterator it = list.begin();
            it != list.end(); ++it)
        addTransmissions( dirName + "/" + it->fileName(), table );
}


// -------------------------------------------------------------------------------------------------
void TransmissionReader::addTransmissions(const QString& fileName, TransmissionTable& table)
    throw (std::invalid_argument, FormatException)
{
    for (uint i = 0; i < m_readers.size(); i++) {
        TransmissionDataReader* reader = m_readers[i];

        if (reader->acceptsFile(fileName))
            reader->addTransmissions(fileName, table);
    }
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
