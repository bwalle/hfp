/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <stdexcept>

#include <QtCore>

#include "transmissionaddxreader.h"
#include "transmission.h"
#include "transmissionreader.h"
#include "charsetconv.h"
#include "time.h"

using std::invalid_argument;

// -------------------------------------------------------------------------------------------------
void TransmissionADDXReader::addTransmissions(const QString& fileName, TransmissionTable& table)
    throw (invalid_argument, FormatException)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
        throw invalid_argument(
            QObject::tr("The file %1 does not exist.").arg(fileName).latin1()
        );

    int size = file.size();
    if (size % 64 != 0)
        throw FormatException("Number of bytes must be a multiple of 4.");

    char* data = new char[size];
    file.readBlock(data, size);

    try {
        QFileInfo fi(fileName);
        Transmission::Language lang = TransmissionReader::fileNameToLanguage(fi.baseName());

        for (int i = 0; i < size; i += 64)
            table.addTransmission( convertToTransmission( (byte*)data + i,
                (byte*)data + i + 64, lang) );
    }
    catch (...) {
        delete data;
        throw;
    }

    delete data;

    file.close();
}


// -------------------------------------------------------------------------------------------------
Transmission TransmissionADDXReader::convertToTransmission(byte* begin, byte* end,
                                                           Transmission::Language lang)
    throw (std::invalid_argument, FormatException)
{
    Transmission newTransmission;

    if (end - begin != 64)
        throw FormatException("Size of a transmission record must be 64.");

    // check first
    if (*(begin+0) != 0x01)
        throw FormatException("First element of a transmission record must be 0x01.");

    // get the beginng time (little endian!)
    int beginTime = (  (byte)*(begin+1) + ((byte)*(begin+2) << 8));
    Time beginqTime(beginTime / 100, beginTime % 100);
    newTransmission.setBeginTime(beginqTime);

    // get the end time (little endian!)
    int endTime = ( (byte)*(begin+3) + ( (byte)*(begin+4) << 8));
    Time endqTime(endTime / 100, endTime % 100);
    newTransmission.setEndTime(endqTime);

    // check
    if (*(begin+5) != 0x03)
        throw FormatException("6th element of a transmission record must be 0x03.");

    // 3 ASCII characters
    char country[4] = { 0, 0, 0, 0 };
    qCopy(begin + 6, begin + 9, country);
    newTransmission.setCountry(QString::fromLatin1(country).stripWhiteSpace());

    // check
    if (*(begin+9) != 0x10)
        throw FormatException("10th element of a transmission record must be 0x10.");

    // get the name of the station
    char station[17];
    memset(station, 0, 17);
    qCopy(begin + 10, begin + 26, station);
    CharsetConverter::cp850ToLatin1(station);
    newTransmission.setStation(QString::fromLatin1(station).stripWhiteSpace());

    // check
    if (*(begin+26) != 0x01 || *(begin+27) != 0x24)
        throw FormatException("26th element of a transmission record must be 0x01, "
            "27th element of a transmission record must be 0x24");

    // frequencies
    char frequencies[37];
    memset(frequencies, 0, 37);
    qCopy(begin + 28, begin + 64, frequencies);
    CharsetConverter::cp850ToLatin1(frequencies);
    newTransmission.setFrequencies(QString::fromLatin1(frequencies).stripWhiteSpace());

    newTransmission.setLanguage(lang);

    return newTransmission;
}


// -------------------------------------------------------------------------------------------------
bool TransmissionADDXReader::acceptsFile(const QString& fileName)
{
    return fileName.lower().endsWith(".dat");
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
