/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef TRANSMISSIONFILTER_H
#define TRANSMISSIONFILTER_H

#include <QtCore>

#include "time.h"
#include "transmission.h"

class TransmissionFilter
{
    public:
        enum TimeFilterType {
            TAll,
            TIsActiveNow,
            TIsActiveAt,
            TBeginsAt,
            TEndsAt
        };

    public:
        TransmissionFilter();

        bool accept(const Transmission& transmission) const;

        // or-ed Transmission::Language
        void setLanguages(int languages);
        void setTime(TimeFilterType type, const Time& time = Time());
        void setStationRegexp(const QRegExp& rx, bool negate);
        void setFrequencyRegexp(const QRegExp& rx, bool negate);
        void setTolerance(uint tolerance);
        void setCountryRegexp(const QRegExp& rx, bool negate);

    private:
        Q_DISABLE_COPY(TransmissionFilter)
        int             m_languages;
        TimeFilterType  m_timeFilterType;
        Time            m_timeValue;
        QRegExp         m_stationRegexp;
        bool            m_stationNegate;
        QRegExp         m_frequencyRegexp;
        bool            m_frequencyNegate;
        uint            m_tolerance;
        QRegExp         m_countryRegexp;
        bool            m_countryNegate;
};

#endif /* TRANSMISSIONFILTER_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
