/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include "global.h"

#include "charsetconv.h"


/* Conversion table generated mechanically by Free `recode' 3.6
   for sequence IBM850..CP1252 (reversible).  */

// -------------------------------------------------------------------------------------------------
static unsigned char const IBM850_CP1252[256] =
{
      0,   1,   2,   3,   4,   5,   6,   7,     /*   0 -   7  */
      8,   9,  10,  11,  12,  13,  14,  15,     /*   8 -  15  */
     16,  17,  18,  19,  20,  21,  22,  23,     /*  16 -  23  */
     24,  25,  26,  27,  28,  29,  30,  31,     /*  24 -  31  */
     32,  33,  34,  35,  36,  37,  38,  39,     /*  32 -  39  */
     40,  41,  42,  43,  44,  45,  46,  47,     /*  40 -  47  */
     48,  49,  50,  51,  52,  53,  54,  55,     /*  48 -  55  */
     56,  57,  58,  59,  60,  61,  62,  63,     /*  56 -  63  */
     64,  65,  66,  67,  68,  69,  70,  71,     /*  64 -  71  */
     72,  73,  74,  75,  76,  77,  78,  79,     /*  72 -  79  */
     80,  81,  82,  83,  84,  85,  86,  87,     /*  80 -  87  */
     88,  89,  90,  91,  92,  93,  94,  95,     /*  88 -  95  */
     96,  97,  98,  99, 100, 101, 102, 103,     /*  96 - 103  */
    104, 105, 106, 107, 108, 109, 110, 111,     /* 104 - 111  */
    112, 113, 114, 115, 116, 117, 118, 119,     /* 112 - 119  */
    120, 121, 122, 123, 124, 125, 126, 127,     /* 120 - 127  */
    199, 252, 233, 226, 228, 224, 229, 231,     /* 128 - 135  */
    234, 235, 232, 239, 238, 236, 196, 197,     /* 136 - 143  */
    201, 230, 198, 244, 246, 242, 251, 249,     /* 144 - 151  */
    255, 214, 220, 248, 163, 216, 215, 131,     /* 152 - 159  */
    225, 237, 243, 250, 241, 209, 170, 186,     /* 160 - 167  */
    191, 174, 172, 189, 188, 161, 171, 187,     /* 168 - 175  */
    155, 157, 141, 129, 139, 193, 194, 192,     /* 176 - 183  */
    169, 150, 132, 140, 148, 162, 165, 151,     /* 184 - 191  */
    156, 145, 147, 128, 142, 143, 227, 195,     /* 192 - 199  */
    159, 144, 146, 133, 138, 153, 158, 164,     /* 200 - 207  */
    240, 208, 202, 203, 200, 134, 205, 206,     /* 208 - 215  */
    207, 137, 130, 136, 154, 166, 204, 152,     /* 216 - 223  */
    211, 223, 212, 210, 245, 213, 181, 254,     /* 224 - 231  */
    222, 218, 219, 217, 253, 221, 175, 180,     /* 232 - 239  */
    173, 177, 149, 190, 182, 167, 247, 184,     /* 240 - 247  */
    176, 168, 183, 185, 179, 178, 135, 160,     /* 248 - 255  */
};


// -------------------------------------------------------------------------------------------------
void CharsetConverter::cp850ToLatin1(char* string)
{
    while (*string) {
        *string = IBM850_CP1252[byte(*string)];
        string++;
    }
}


// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
