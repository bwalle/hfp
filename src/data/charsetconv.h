/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef CHARSETCONV_H
#define CHARSETCONV_H


class CharsetConverter
{
    public:
        static void cp850ToLatin1(char* string);

    private:
        CharsetConverter() {}
        virtual ~CharsetConverter() {}
        Q_DISABLE_COPY(CharsetConverter)
};

#endif /* CHARSETCONV_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
