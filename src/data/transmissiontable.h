/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef TRANSMISSIONTABLE_H
#define TRANSMISSIONTABLE_H

#include <stdexcept>

#include "transmission.h"
#include "global.h"
#include "transmissionfilter.h"

class TransmissionTable
{
    public:
        TransmissionTable() {}

    public:

        void clear();

        size_t size() const;

        Transmission getTransmission(int i) const;
        void addTransmission(const Transmission& transmission);

    private:
        Q_DISABLE_COPY(TransmissionTable)
        Q3ValueVector<Transmission> m_transmissions;
};


#endif /* TRANSMISSIONTABLE_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
