/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#ifndef CENTRALWIDGET_H
#define CENTRALWIDGET_H

#include <QtGui>
#include <Qt3Support>

#include "data/transmissionfilter.h"

#include "filterpanel.h"
#include "stationlist.h"

class CentralWidget : public Q3Frame
{
    Q_OBJECT

    public:
        CentralWidget(QWidget* parent);

        TransmissionFilter& getTransmissionFilter();

        StationList* getStationList() const;

    public slots:
        void loadTransmissionTable();

    private:
        Q_DISABLE_COPY(CentralWidget)
        StationList*            m_stationList;
        FilterPanel*            m_filterPanel;
        TransmissionFilter      m_transmissionFilter;
};



#endif /* CENTRALWIDGET_H */

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
