/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QtGui>
#include <Qt3Support>

#include "itudialog.h"
#include "data/itu.h"
#include "global.h"

// -------------------------------------------------------------------------------------------------
ItuDialog::ItuDialog(QWidget* parent, const char* name = 0)
    : QDialog(parent, name)
{
    Q3VBoxLayout* mainLayout = new Q3VBoxLayout(this, 10, 6);
    setCaption(tr("ITU Countries"));

    // the line edit
    Q3HBox* filterBox = new Q3HBox(this);
    filterBox->setSpacing(10);
    QLabel* filterLabel = new QLabel(tr("&Filter:"), filterBox);
    m_filterEdit = new QLineEdit(filterBox);
    filterLabel->setBuddy(m_filterEdit);

    // the list
    m_listview = new Q3ListView(this, "ITU Listview");
    m_listview->addColumn(tr("ITU"), 50);
    m_listview->addColumn(tr("Country"), 400);
    m_listview->setResizeMode(Q3ListView::LastColumn);
    m_listview->header()->setMovingEnabled(false);
    m_listview->setAllColumnsShowFocus(true);
    m_listview->setShowSortIndicator(true);

    // the ok Button
    QPushButton* okButton = new QPushButton(tr("&Close"), this, "Close button");
    okButton->setDefault(false);
    QWidget* filler = new QWidget(this);
    Q3HBoxLayout* buttonLayout = new Q3HBoxLayout(0, 0, 6);
    buttonLayout->addWidget(filler);
    buttonLayout->addWidget(okButton);
    buttonLayout->setStretchFactor(filler, 1);
    buttonLayout->setStretchFactor(okButton, 0);

    // main layout
    mainLayout->addWidget(filterBox);
    mainLayout->addWidget(m_listview);
    mainLayout->addSpacing(5);
    mainLayout->addLayout(buttonLayout);

    resize( int(parent->width() * 0.8), int(parent->height() * 0.6) );
    updateItus();

    connect(okButton, SIGNAL(clicked()), SLOT(accept()));
    connect(m_filterEdit, SIGNAL(returnPressed()), SLOT(updateItus()));
    connect(m_listview, SIGNAL(mouseButtonClicked(int, Q3ListViewItem*, const QPoint&, int)),
        SLOT(mouseButtonClickedHandler(int, Q3ListViewItem*, const QPoint&, int)));

}


// -------------------------------------------------------------------------------------------------
void ItuDialog::updateItus()
{
    m_listview->clear();

    StringList itus = ITU::getITUs();

    QRegExp rx(m_filterEdit->text());
    rx.setCaseSensitive(false);
    if (!rx.isValid()) {
        QMessageBox::information(this, "HFP",
            tr("The regular expression for the country is not valid:\n\n%1").arg(
                rx.errorString()),
            QMessageBox::Ok, QMessageBox::NoButton);
        return;
    }

    for (StringList::iterator it = itus.begin(); it != itus.end(); ++it) {
        QString itu = *it;
        QString country = ITU::getCountry(*it);

        if (rx.search(country) != -1)
            (void) new Q3ListViewItem(m_listview, itu, country);
    }
}


// -------------------------------------------------------------------------------------------------
void ItuDialog::keyPressEvent(QKeyEvent* e)
{
    if (!(e->key() == Qt::Key_Return && m_filterEdit->hasFocus()))
        QDialog::keyPressEvent(e);
}


// -------------------------------------------------------------------------------------------------
void ItuDialog::mouseButtonClickedHandler(int button, Q3ListViewItem* item,
                                          const QPoint & pos, int c)
{
    if (item && (button & Qt::MidButton)) {
        QString text = item->text(c);

        QClipboard* clip = QApplication::clipboard();
        clip->setText(text, QClipboard::Clipboard);
        if (clip->supportsSelection())
            clip->setText(text, QClipboard::Selection);
    }
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
