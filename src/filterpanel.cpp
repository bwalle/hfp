/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QtGui>
#include <Qt3Support>

#include "time.h"
#include "centralwidget.h"
#include "filterpanel.h"
#include "settings.h"

FilterPanel::FilterPanel(QWidget* parent, CentralWidget* centralWidget)
    : Q3Frame(parent, "Filter panel")
    , m_centralWidget(centralWidget)
{
    Q3GridLayout* layout = new Q3GridLayout(this, 4, 5, 0 /* margin */, 5);

    QLabel* languageLabel   = new QLabel(tr("Languages:"), this);
    Q3HBox*  languagesBox   = new Q3HBox(this);
    m_germanCheckBox        = new QCheckBox(tr("&German"), languagesBox);
    m_englishCheckBox       = new QCheckBox(tr("&English"), languagesBox);
    m_frenchCheckBox        = new QCheckBox(tr("&French"), languagesBox);
    m_spanishCheckBox       = new QCheckBox(tr("&Spanish"), languagesBox);
    m_russianCheckBox       = new QCheckBox(tr("&Russian"), languagesBox);
    m_turkishCheckBox       = new QCheckBox(tr("&Turkish"), languagesBox);
    QWidget* filler         = new QWidget(languagesBox);
    languagesBox->setStretchFactor(filler, 10);
    languagesBox->setSpacing(5);


    QLabel*    timeLabel    = new QLabel(tr("T&ime:"), this);
    Q3HBox*    timeBox      = new Q3HBox(this);
    m_timeCombo             = new QComboBox(timeBox);
    m_timeEdit              = new Q3TimeEdit(timeBox);
    m_toleranceCheckBox     = new QCheckBox(tr("Toleran&ce:"), timeBox);
    m_toleranceSpinner      = new QSpinBox(0, 60*24, 1, timeBox);
    QWidget*   filler2      = new QWidget(timeBox);
    timeBox->setStretchFactor(filler2, 10);
    timeBox->setSpacing(5);
    timeLabel->setBuddy(m_timeCombo);

    // insert the values in the combo box, the order must be compatbile with
    // TransmissionFilter::TimeFilterType
    m_timeCombo->insertItem(tr("All"));
    m_timeCombo->insertItem(tr("Is active now"));
    m_timeCombo->insertItem(tr("Is active at"));
    m_timeCombo->insertItem(tr("Begins at"));
    m_timeCombo->insertItem(tr("Ends at"));

    m_timeEdit->setDisplay(Q3TimeEdit::Hours | Q3TimeEdit::Minutes);


    // the country ---------------------------------------------------------------------------------
    QLabel*   countryLabel  = new QLabel(tr("C&ountry:"), this);
    Q3HBox*   countryBox    = new Q3HBox(this);
    m_countryEdit           = new QLineEdit(countryBox);
    m_countryNegateCheckBox = new QCheckBox(tr("Negate"), countryBox);
    countryLabel->setBuddy(m_countryEdit);
    countryBox->setSpacing(5);

    // the station ---------------------------------------------------------------------------------
    QLabel*   stationLabel  = new QLabel(tr("St&ations:"), this);
    Q3HBox*   stationBox    = new Q3HBox(this);
    m_stationEdit           = new QLineEdit(stationBox);
    m_statNegateCheckBox    = new QCheckBox(tr("Negate"), stationBox);
    stationLabel->setBuddy(m_stationEdit);
    stationBox->setSpacing(5);

    // the frequencies -----------------------------------------------------------------------------
    QLabel*   frequLabel    = new QLabel(tr("Fre&quencies:"), this);
    Q3HBox*   frequBox      = new Q3HBox(this);
    m_frequencyEdit         = new QLineEdit(frequBox);
    m_frequNegateCheckBox   = new QCheckBox(tr("Negate"), frequBox);
    frequLabel->setBuddy(m_frequencyEdit);
    frequBox->setSpacing(5);

    // - buttons -----------------------------------------------------------------------------------

    // box
    Q3VBox*    buttonsBox    = new Q3VBox(this);
    buttonsBox->setSpacing(5);

    // the apply button
    m_applyButton = new QToolButton(buttonsBox);
    m_applyButton->setIcon(QIcon(QPixmap(":/images/stock_apply_20.png")));
    QToolTip::add(m_applyButton, tr("Apply filter"));
    m_applyButton->setIconSize(QSize(22, 22));

    // save button
    m_saveButton = new QToolButton(buttonsBox);
    m_saveButton->setIcon(QIcon(QPixmap(":/images/stock_save_16.png")));
    QToolTip::add(m_saveButton, tr("Save as favourite"));
    m_saveButton->setIconSize(QSize(22, 22));

    // restore button
    m_restoreButton = new QToolButton(buttonsBox);
    m_restoreButton->setIcon(QIcon(QPixmap(":/images/stock_revert_16.png")));
    QToolTip::add(m_restoreButton, tr("Restore favourite"));
    m_restoreButton->setIconSize(QSize(22, 22));

    // clean button
    m_cleanButton = new QToolButton(buttonsBox);
    m_cleanButton->setIcon(QIcon(QPixmap(":/images/stock_clear_24.png")));
    QToolTip::add(m_cleanButton, tr("Clear filter entries"));
    m_cleanButton->setIconSize(QSize(22, 22));


    // - layout the stuff --------------------------------------------------------------------------

    // spacing
    layout->setColSpacing(1, 20);
    layout->setColSpacing(3, 20);

    // filter widgets
    layout->addWidget(languageLabel, 0, 0);
    layout->addWidget(languagesBox, 0, 2);
    layout->addWidget(timeLabel, 1, 0);
    layout->addWidget(timeBox, 1, 2);
    layout->addWidget(countryLabel, 2, 0);
    layout->addWidget(countryBox, 2, 2);
    layout->addWidget(stationLabel, 3, 0);
    layout->addWidget(stationBox, 3, 2);
    layout->addWidget(frequLabel, 4, 0);
    layout->addWidget(frequBox, 4, 2);

    // buttons
    layout->addMultiCellWidget(buttonsBox, 0, 4, 4, 4, Qt::AlignTop);

    // - connect signals and slots -----------------------------------------------------------------

    // ENTER
    connect(m_stationEdit, SIGNAL(returnPressed()), SLOT(updateFilter()));
    connect(m_frequencyEdit, SIGNAL(returnPressed()), SLOT(updateFilter()));
    connect(m_countryEdit, SIGNAL(returnPressed()), SLOT(updateFilter()));

    // disable widgets
    connect(m_timeCombo, SIGNAL(activated(int)), SLOT(updateTimeEdit(int)));
    connect(m_toleranceCheckBox, SIGNAL(toggled(bool)), SLOT(updateSpinner(bool)));

    // buttons
    connect(m_applyButton, SIGNAL(clicked()), SLOT(updateFilter()));
    connect(m_saveButton, SIGNAL(clicked()), SLOT(save()));
    connect(m_restoreButton, SIGNAL(clicked()), SLOT(restore()));
    connect(m_cleanButton, SIGNAL(clicked()), SLOT(clean()));

    // - load settings -----------------------------------------------------------------------------

    restoreSettings("default");
    updateTimeEdit(m_timeCombo->currentItem());
    updateSpinner(m_toleranceCheckBox->isChecked());
    updateFilter();
}


// -------------------------------------------------------------------------------------------------
FilterPanel::~FilterPanel()
{
    saveSettings("default");
}


// -------------------------------------------------------------------------------------------------
void FilterPanel::saveSettings(const QString& name)
{
    Settings& set = Settings::set();

    set.writeEntry("Filter/settingsStored" + name, true);
    set.writeEntry("Filter/German" + name, m_germanCheckBox->isChecked());
    set.writeEntry("Filter/English" + name, m_englishCheckBox->isChecked());
    set.writeEntry("Filter/French" + name, m_frenchCheckBox->isChecked());
    set.writeEntry("Filter/Russian" + name, m_russianCheckBox->isChecked());
    set.writeEntry("Filter/Turkish" + name, m_turkishCheckBox->isChecked());
    set.writeEntry("Filter/Spanish" + name, m_spanishCheckBox->isChecked());
    set.writeEntry("Filter/TimeCombo" + name, m_timeCombo->currentItem());
    set.writeEntry("Filter/TimeValue" + name, m_timeEdit->time().toString("hh:mm"));
    set.writeEntry("Filter/Stations" + name, m_stationEdit->text());
    set.writeEntry("Filter/Frequencies" + name, m_frequencyEdit->text());
    set.writeEntry("Filter/StationNegate" + name, m_statNegateCheckBox->isChecked());
    set.writeEntry("Filter/FrequencyNegate" + name, m_frequNegateCheckBox->isChecked());
    set.writeEntry("Filter/ToleranceChecked" + name, m_toleranceCheckBox->isChecked());
    set.writeEntry("Filter/Tolerance" + name, m_toleranceSpinner->value());
    set.writeEntry("Filter/Country" + name, m_countryEdit->text());
    set.writeEntry("Filter/CountryNegate" + name, m_countryNegateCheckBox->isChecked());
}


// -------------------------------------------------------------------------------------------------
void FilterPanel::restoreSettings(const QString& name)
{
    if (Settings::set().readBoolEntry("Filter/settingsStored" + name))
    {
        Settings& set = Settings::set();

        m_germanCheckBox->setChecked(set.readBoolEntry("Filter/German" + name));
        m_englishCheckBox->setChecked(set.readBoolEntry("Filter/English" + name));
        m_frenchCheckBox->setChecked(set.readBoolEntry("Filter/French" + name));
        m_spanishCheckBox->setChecked(set.readBoolEntry("Filter/Spanish" + name));
        m_russianCheckBox->setChecked(set.readBoolEntry("Filter/Russian" + name));
        m_turkishCheckBox->setChecked(set.readBoolEntry("Filter/Turkish" + name));

        m_timeCombo->setCurrentItem(set.readNumEntry("Filter/TimeCombo" + name));
        m_timeEdit->setTime(QTime::fromString(set.readEntry("Filter/TimeValue" + name)));
        m_stationEdit->setText(set.readEntry("Filter/Stations" + name));
        m_frequencyEdit->setText(set.readEntry("Filter/Frequencies" + name));
        m_toleranceCheckBox->setChecked(set.readBoolEntry("Filter/ToleranceChecked" + name));
        m_toleranceSpinner->setValue(set.readNumEntry("Filter/Tolerance" + name));
        m_countryEdit->setText(set.readEntry("Filter/Country" + name));

        m_statNegateCheckBox->setChecked(set.readBoolEntry("Filter/StationNegate" + name));
        m_frequNegateCheckBox->setChecked(set.readBoolEntry("Filter/FrequencyNegate" + name));
        m_countryNegateCheckBox->setChecked(set.readBoolEntry("Filter/CountryNegate" + name));
    }
    else
    {
        m_germanCheckBox->setChecked(true);
        m_englishCheckBox->setChecked(true);
    }
}


// -------------------------------------------------------------------------------------------------
void FilterPanel::updateTimeEdit(int item)
{
    TransmissionFilter::TimeFilterType type = (TransmissionFilter::TimeFilterType)item;

    m_timeEdit->setEnabled(type == TransmissionFilter::TBeginsAt
        || type == TransmissionFilter::TEndsAt || type == TransmissionFilter::TIsActiveAt);
    m_toleranceCheckBox->setEnabled(type != TransmissionFilter::TAll);
    m_toleranceSpinner->setEnabled(type != TransmissionFilter::TAll);
}


// -------------------------------------------------------------------------------------------------
void FilterPanel::updateSpinner(bool on)
{
    m_toleranceSpinner->setEnabled(on);
}


// -------------------------------------------------------------------------------------------------
void FilterPanel::updateFilter()
{
    TransmissionFilter& filter = m_centralWidget->getTransmissionFilter();

    // check the regular expression
    QRegExp rxStation(m_stationEdit->text());
    rxStation.setCaseSensitive(false);
    if (!rxStation.isValid()) {
        QMessageBox::information(this, "HFP",
            tr("The regular expression for the station is not valid:\n\n%1").arg(
                rxStation.errorString()),
            QMessageBox::Ok, QMessageBox::NoButton);
        return;
    }

    QRegExp rxFreuqu(m_frequencyEdit->text());
    rxFreuqu.setCaseSensitive(false);
    if (!rxFreuqu.isValid()) {
        QMessageBox::information(this, "HFP",
            tr("The regular expression for the frequencies is not valid:\n\n%1").arg(
                rxFreuqu.errorString()),
            QMessageBox::Ok, QMessageBox::NoButton);
        return;
    }

    QRegExp rxCountry(m_countryEdit->text());
    rxCountry.setCaseSensitive(false);
    if (!rxCountry.isValid()) {
        QMessageBox::information(this, "HFP",
            tr("The regular expression for the country is not valid:\n\n%1").arg(
                rxFreuqu.errorString()),
            QMessageBox::Ok, QMessageBox::NoButton);
        return;
    }

    filter.setStationRegexp(rxStation, m_statNegateCheckBox->isChecked());
    filter.setFrequencyRegexp(rxFreuqu, m_frequNegateCheckBox->isChecked());
    filter.setCountryRegexp(rxCountry, m_countryNegateCheckBox->isChecked());
    filter.setTolerance( m_toleranceCheckBox->isChecked() ? m_toleranceSpinner->value() : 0 );

    int languages = 0;
    if (m_germanCheckBox->isChecked())
        languages |= Transmission::LGerman;
    if (m_englishCheckBox->isChecked())
        languages |= Transmission::LEnglish;
    if (m_frenchCheckBox->isChecked())
        languages |= Transmission::LFrench;
    if (m_spanishCheckBox->isChecked())
        languages |= Transmission::LSpanish;
    if (m_turkishCheckBox->isChecked())
        languages |= Transmission::LTurkish;
    if (m_russianCheckBox->isChecked())
        languages |= Transmission::LRussian;
    filter.setLanguages(languages);
    filter.setTime( TransmissionFilter::TimeFilterType(m_timeCombo->currentItem()),
        static_cast<Time>(m_timeEdit->time()));

    emit filterChanged();
}


// -------------------------------------------------------------------------------------------------
void FilterPanel::save()
{
    saveSettings("favourite");
}


// -------------------------------------------------------------------------------------------------
void FilterPanel::restore()
{
    restoreSettings("favourite");

    updateTimeEdit(m_timeCombo->currentItem());
    updateSpinner(m_toleranceCheckBox->isChecked());
    updateFilter();
}


// -------------------------------------------------------------------------------------------------
void FilterPanel::clean()
{
    m_frequNegateCheckBox->setChecked(false);
    m_statNegateCheckBox->setChecked(false);
    m_countryNegateCheckBox->setChecked(false);
    m_timeCombo->setCurrentItem(0);
    m_toleranceCheckBox->setChecked(false);
    m_frequencyEdit->setText("");
    m_stationEdit->setText("");
    m_countryEdit->setText("");

    updateTimeEdit(m_timeCombo->currentItem());
    updateSpinner(m_toleranceCheckBox->isChecked());
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
