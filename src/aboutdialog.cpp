/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include "aboutdialog.h"

#include <QtGui>
#include <Qt3Support>

// -------------------------------------------------------------------------------------------------
AboutDialog::AboutDialog(QWidget* parent, const char* name)
    : QDialog(parent, name)
{
    m_mainLayout = new QVBoxLayout(this, 10, 6);
    setCaption(tr("About HFP"));

    // the top of the dialog
    Q3HBox* titleBox = new Q3HBox(this);
    QLabel* titleIcon = new QLabel(titleBox);
    titleIcon->setPixmap(QPixmap(":/images/hfp_48.png"));
    QLabel* titleText = new QLabel(tr("<p><b>HFP "VERSION_STRING"</b></p>"), titleBox);
    titleBox->setStretchFactor(titleIcon, 0);
    titleBox->setStretchFactor(titleText, 5);
    titleBox->setSpacing(5);

    // the tab
    m_tabWidget = new QTabWidget(this);
    setupAboutTab();
    setupLicenseTab();

    // the ok Button
    QPushButton* okButton = new QPushButton(tr("&Close"), this, "Close button");
    okButton->setDefault(true);
    QWidget* filler = new QWidget(this);
    Q3HBoxLayout* buttonLayout = new Q3HBoxLayout(0, 0, 6);
    buttonLayout->addWidget(filler);
    buttonLayout->addWidget(okButton);
    buttonLayout->setStretchFactor(filler, 1);
    buttonLayout->setStretchFactor(okButton, 0);

    // main layout
    m_mainLayout->addWidget(titleBox);
    m_mainLayout->addWidget(m_tabWidget);
    m_mainLayout->setStretchFactor(m_tabWidget, 5);
    m_mainLayout->addLayout(buttonLayout);

    connect(okButton, SIGNAL(clicked()), SLOT(accept()));

}


// -------------------------------------------------------------------------------------------------
void AboutDialog::setupAboutTab()
{
    Q3HBox* aboutTab = new Q3HBox(this);
    aboutTab->setMargin(15);

    QLabel *label = new QLabel(
        tr("<p><nobr>This is a station schedule managing tool for Unix and "
            "MacOS X</nobr> written in C++ using the Qt library.</p>"
            "<p><b>Thanks to:</b>"
            "<ul><li>Trolltech for the Qt library</li>"
            "<li>ADDX e.V. (<tt>http://www.addx.de</tt>) for providing the schedules</li>"
            "<li>Gtk+ artists for the nice stock icons</li>"
            "</ul></p>"
            "<p><b>Homepage:</b> <tt>http://www.bwalle.de/hfp/</tt></p>"), aboutTab);
    label->setAlignment(Qt::AlignTop);

    m_tabWidget->addTab(aboutTab, tr("&About"));

}


// -------------------------------------------------------------------------------------------------
void AboutDialog::setupLicenseTab()
{
    Q3VBox* licenseTab = new Q3VBox(this);

    Q3TextEdit* textEdit = new Q3TextEdit(licenseTab);
    textEdit->setReadOnly(true);
    textEdit->setWordWrap(Q3TextEdit::FixedColumnWidth);
    textEdit->setWrapColumnOrWidth(100);

    QFile file(":/COPYING");
    if (file.open( QIODevice::ReadOnly )) {
        Q3TextStream stream(&file);
        textEdit->setText("<pre>" + stream.read() + "</pre>");
    }

    m_tabWidget->addTab(licenseTab, tr("&License"));
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
