/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QString>

#include "stationlistviewitem.h"

// -------------------------------------------------------------------------------------------------
QString StationListViewItem::text(int column) const
{
    switch (column) {
        case 0:
            return m_transmission.getBeginTimeAsString();
        case 1:
            return m_transmission.getEndTimeAsString();
        case 2:
            return m_transmission.getLanguageAsString();
        case 3:
            return m_transmission.getCountry();
        case 4:
            return m_transmission.getStation();
        case 5:
            return m_transmission.getFrequencies();
    }

    return "";
}


// -------------------------------------------------------------------------------------------------
Transmission StationListViewItem::getTransmission() const
{
    return m_transmission;
}


// -------------------------------------------------------------------------------------------------
int StationListViewItem::compare(Q3ListViewItem *i, int col, bool ascending) const
{
    switch (col) {
        case 0:
            return m_transmission.getBeginTime().compare(
                    static_cast<StationListViewItem*>(i)->m_transmission.getBeginTime());
        case 1:
            return m_transmission.getEndTime().compare(
                    static_cast<StationListViewItem*>(i)->m_transmission.getEndTime());

        default:
            return key( col, ascending ).compare( i->key( col, ascending) );
    }
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
