/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <iostream>
#include <stdexcept>
#include <cstdlib>
#include <cstdio>
#include <memory>

#include <QtGui>
#include <Qt3Support>

#include "global.h"
#include "settings.h"
#include "hfp.h"
#include "data/transmissiontable.h"

// -------------------------------------------------------------------------------------------------
void printCommandlineOptions()
{
    std::cerr
        << "\n"
        << "This is HFP " << VERSION_STRING << ".\n"
        << "Options:\n"
        << "     --help             prints this help\n"
        << "     --version          prints version info\n"
        << "     --print-update-url prints the update URL, needed for automatic update\n"
        << std::endl;
}

// -------------------------------------------------------------------------------------------------
void printVersion()
{
    std::cerr << "HFP version   " << VERSION_STRING << std::endl;
}

// -------------------------------------------------------------------------------------------------
void parseCommandLine(int argc, char *argv[])
{
    for (int i = 1; i < argc; ++i) {
        QString string = QString::fromLatin1(argv[i]);
        if (string == "-h" || string == "--help" || string == "-help") {
            printCommandlineOptions();
            exit(0);
        } else if (string == "-v" || string == "--version" || string == "-version") {
            printVersion();
            exit(0);
        } else if (string == "--print-update-url") {
            std::cout << Settings::set().readEntry("General/ADDX_URL").latin1() << std::endl;
            std::exit(0);
        }
    }
}

// -------------------------------------------------------------------------------------------------
int main(int argc, char *argv[])
{
#ifdef Q_WS_MAC
    QFont::insertSubstitution(".Lucida Grande UI", "Lucida Grande");
#endif
    
    QApplication app(argc, argv);
    parseCommandLine(argc, argv);

    // translation
    QTranslator translator(0), ttranslator(0);
    QString loc = QTextCodec::locale();
    if (!translator.load(loc, "."))
        translator.load(loc, qApp->applicationDirPath() + "/../share/hfp/translations/");
    if (!ttranslator.load(QString("qt_") + loc, "/usr/share/qt4/translations/"))
        ttranslator.load(QString("qt_") + loc, "/usr/share/qt/translations/");
    app.installTranslator(&translator);
    app.installTranslator(&ttranslator);

    try {
        std::auto_ptr<Hfp> hfp(new Hfp);

        app.setMainWidget(hfp.get());

        hfp->show();
        app.connect(&app, SIGNAL(lastWindowClosed()), &app, SLOT(quit()));

        return app.exec();
    } catch (const std::bad_alloc &e) {
        Q_UNUSED(e);
        QMessageBox::warning(0, QObject::tr("HFP"),
            QObject::tr("No more memory available. The application\nwill be closed."),
            QMessageBox::Ok | QMessageBox::Default, QMessageBox::NoButton);
    } catch (const std::exception &e) {
        QMessageBox::warning(0, QObject::tr("HFP"),
            QObject::tr("An unknown error occurred:\n%1\nThe application will be closed.")
            .arg(e.what()),
            QMessageBox::Ok | QMessageBox::Default, QMessageBox::NoButton);
    }
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
