/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QtGui>
#include <Qt3Support>

#include "centralwidget.h"
#include "stationlist.h"
#include "filterpanel.h"
#include "hfp.h"

// -------------------------------------------------------------------------------------------------
CentralWidget::CentralWidget(QWidget* parent) : Q3Frame(parent, "CentralWidget")
{
    Q3VBoxLayout* layout = new Q3VBoxLayout(this, 6);
    m_stationList = new StationList(this, this);
    layout->addWidget(m_stationList);
    layout->setStretchFactor(m_stationList, 10);

    Q3GroupBox* groupBox = new Q3GroupBox(1, Qt::Horizontal, this);
    m_filterPanel = new FilterPanel(groupBox, this);

    layout->addWidget(groupBox);
    layout->setStretchFactor(groupBox, 0);

    connect(m_filterPanel, SIGNAL(filterChanged()), m_stationList, SLOT(updateView()));
    connect(static_cast<Hfp*>(parent), SIGNAL(reloadData()),
        m_stationList, SLOT(loadTransmissionTable()));
}


// -------------------------------------------------------------------------------------------------
void CentralWidget::loadTransmissionTable()
{
    m_stationList->loadTransmissionTable();
}


// -------------------------------------------------------------------------------------------------
TransmissionFilter& CentralWidget::getTransmissionFilter()
{
    return m_transmissionFilter;
}


// -------------------------------------------------------------------------------------------------
StationList* CentralWidget::getStationList() const
{
    return m_stationList;
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
