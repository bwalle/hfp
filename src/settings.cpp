/*
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; You may only use
 * version 2 of the License, you have no option to use any other version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 * the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * -------------------------------------------------------------------------------------------------
 */
#include <QtCore>

#include "global.h"
#include "settings.h"

// -------------------------------------------------------------------------------------------------
Settings::Settings()
{
    m_qSettings.setPath( "bwalle.de", "hfp", QSettings::User );
    m_qSettings.beginGroup("/hfp");

#define DEF_STRING(a, b) ( m_stringMap.insert( (a), (b) ) )
#define DEF_INTEGE(a, b) ( m_intMap.insert( (a), (b) ) )
#define DEF_DOUBLE(a, b) ( m_doubleMap.insert( (a), (b) ) )
#define DEF_BOOLEA(a, b) ( m_boolMap.insert( (a), (b) ) )

    DEF_BOOLEA("Main Window/maximized",          false);
    DEF_BOOLEA("General/AutoUpdate",             false);
    DEF_STRING("General/Datadir",                QDir::homeDirPath() + "/.hfp/");
    DEF_STRING("General/ADDX_URL",               "http://www.addx.de/Hfpdat/hfps.zip");

#undef DEF_STRING
#undef DEF_INTEGE
#undef DEF_DOUBLE
#undef DEF_BOOLEA
}


// -------------------------------------------------------------------------------------------------
bool Settings::writeEntry(const QString & key, const QString & value)
{
    return m_qSettings.writeEntry(key, value);
}


// -------------------------------------------------------------------------------------------------
bool Settings::writeEntry(const QString & key, double value)
{
    return m_qSettings.writeEntry(key, value);
}


// -------------------------------------------------------------------------------------------------
bool Settings::writeEntry(const QString & key, int value)
{
    return m_qSettings.writeEntry(key, value);
}


// -------------------------------------------------------------------------------------------------
bool Settings::writeEntry(const QString & key, bool value)
{
    return m_qSettings.writeEntry(key, value);
}

// -------------------------------------------------------------------------------------------------
bool Settings::writeEntry(const QString& key, const QByteArray& bytes)
{
    return m_qSettings.writeEntry(key, QString::fromUtf8(bytes.toBase64()));

}

// -------------------------------------------------------------------------------------------------
QString Settings::readEntry(const QString & key, const QString& def)
{
    bool read = false;
    QString string = m_qSettings.readEntry(key, def, &read);
    if (!read && m_stringMap.contains(key))
        return m_stringMap[key];
#ifdef DEBUG
    else if (!read && def.isNull())
        PRINT_DBG("Implicit default returned, key = %s", key.latin1());
#endif
    return string;
}


// -------------------------------------------------------------------------------------------------
int Settings::readNumEntry (const QString & key, int def)
{
    bool read = false;
    int number = m_qSettings.readNumEntry(key, def, &read);
    if (!read && m_intMap.contains(key))
        return m_intMap[key];
#ifdef DEBUG
    else if (!read && def == 0)
        PRINT_TRACE("Implicit default returned, key = %s", key.latin1());
#endif
    return number;
}


// -------------------------------------------------------------------------------------------------
double Settings::readDoubleEntry(const QString & key, double def)
{
    bool read = false;
    double number = m_qSettings.readDoubleEntry(key, def, &read);
    if (!read && m_doubleMap.contains(key))
        return m_doubleMap[key];
#ifdef DEBUG
    else if (!read && def == 0.0)
        PRINT_TRACE("Implicit default returned, key = %s", key.latin1());
#endif
    return number;
}


// -------------------------------------------------------------------------------------------------
bool Settings::readBoolEntry(const QString & key, bool def)
{
    bool read = false;
    bool res = m_qSettings.readBoolEntry(key, def, &read);
    if (!read && m_boolMap.contains(key))
        return m_boolMap[key];
#ifdef DEBUG
    else if (!read && !def)
        PRINT_TRACE("Implicit default returned, key = %s", key.latin1());
#endif
    return res;
}

// -------------------------------------------------------------------------------------------------
QByteArray Settings::readByteArrayEntry(const QString& key, const QByteArray& def)
{
    if (m_qSettings.contains(key))
        return QByteArray::fromBase64(m_qSettings.value(key).toString().toUtf8());
    else
        return def;
}

// -------------------------------------------------------------------------------------------------
Settings& Settings::set()
{
    static Settings instanz;

    return instanz;
}

// :maxLineLen=100:tabSize=4:indentSize=4:noTabs=true:
