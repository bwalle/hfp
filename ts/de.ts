<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<defaultcodec></defaultcodec>
<context>
    <name></name>
    <message>
        <location filename="../src/data/itu.cpp" line="24"/>
        <source>Egypt</source>
        <translation>Ägypten</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="25"/>
        <source>Equatorial Guinea</source>
        <translation>Äquatorial-Guinea</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="26"/>
        <source>Ethiopia</source>
        <translation>Äthiopien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="27"/>
        <source>Afghanistan</source>
        <translation>Afghanistan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="28"/>
        <source>Alaska</source>
        <translation>Alaska</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="29"/>
        <source>Albania</source>
        <translation>Albanien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="30"/>
        <source>Algeria</source>
        <translation>Algerien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="31"/>
        <source>Andorra</source>
        <translation>Andorra</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="32"/>
        <source>Angola</source>
        <translation>Angola</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="33"/>
        <source>Anguilla</source>
        <translation>Anguilla</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="34"/>
        <source>Antarctica</source>
        <translation>Antarktis</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="35"/>
        <source>Antigua</source>
        <translation>Antigua</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="36"/>
        <source>Argentina</source>
        <translation>Argentinien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="37"/>
        <source>Armenia</source>
        <translation>Armenien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="38"/>
        <source>Ascension</source>
        <translation>Ascension</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="39"/>
        <source>Azerbaijan</source>
        <translation>Aserbaidschan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="40"/>
        <source>Australia</source>
        <translation>Australien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="41"/>
        <source>Azores</source>
        <translation>Azoren</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="42"/>
        <source>Bahamas</source>
        <translation>Bahamas</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="43"/>
        <source>Bahrain</source>
        <translation>Bahrain</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="44"/>
        <source>Bangladesh</source>
        <translation>Bangladesch</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="45"/>
        <source>Barbados</source>
        <translation>Barbados</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="46"/>
        <source>Belarus</source>
        <translation>Belarus (Weißrussland)</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="47"/>
        <source>Belgium</source>
        <translation>Belgien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="48"/>
        <source>Belize</source>
        <translation>Belize</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="49"/>
        <source>Benin</source>
        <translation>Benin</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="50"/>
        <source>Bhutan</source>
        <translation>Bhutan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="51"/>
        <source>Bolivia</source>
        <translation>Bolivien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="52"/>
        <source>Bosnia and Herzegovina</source>
        <translation>Bosnien-Herzegowina</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="53"/>
        <source>Botswana</source>
        <translation>Botswana</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="54"/>
        <source>Brasilia</source>
        <translation>Brasilien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="55"/>
        <source>Brunei</source>
        <translation>Brunei</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="56"/>
        <source>Bulgaria</source>
        <translation>Bulgarien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="57"/>
        <source>Burkina Faso</source>
        <translation>Burkina Faso</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="58"/>
        <source>Burundi</source>
        <translation>Burundi</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="59"/>
        <source>Chile</source>
        <translation>Chile</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="60"/>
        <source>China</source>
        <translation>China</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="61"/>
        <source>Costa Rica</source>
        <translation>Costa Rica</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="62"/>
        <source>Cote d&apos;Ivoire</source>
        <translation>Cote d&apos;Ivoire (Elfenbeinküste)</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="63"/>
        <source>Denmark</source>
        <translation>Dänemark</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="64"/>
        <source>Germany</source>
        <translation>Deutschland</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="65"/>
        <source>Diego García</source>
        <translation>Diego García</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="66"/>
        <source>Dominica</source>
        <translation>Dominica</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="67"/>
        <source>Dominican Republic</source>
        <translation>Dominikanische Republik</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="68"/>
        <source>Dschibuti</source>
        <translation>Dschibuti</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="69"/>
        <source>Ecuador</source>
        <translation>Ecuador</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="70"/>
        <source>El Salvador</source>
        <translation>El Salvador</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="71"/>
        <source>Eritrea</source>
        <translation>Eritrea</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="72"/>
        <source>Estonia</source>
        <translation>Estland</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="73"/>
        <source>Faroe islands</source>
        <translation>Faroer-Inseln</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="74"/>
        <source>Fiji</source>
        <translation>Fidschi</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="75"/>
        <source>Finland</source>
        <translation>Finnland</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="76"/>
        <source>France</source>
        <translation>Frankreich</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="77"/>
        <source>Gabun</source>
        <translation>Gabun</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="78"/>
        <source>Gambia</source>
        <translation>Gambia</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="79"/>
        <source>Georgia</source>
        <translation>Georgien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="80"/>
        <source>Ghana</source>
        <translation>Ghana</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="81"/>
        <source>Gibraltar</source>
        <translation>Gibraltar</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="82"/>
        <source>Grenada</source>
        <translation>Grenada</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="83"/>
        <source>Greece</source>
        <translation>Griechenland</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="84"/>
        <source>Greenland</source>
        <translation>Grönland</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="85"/>
        <source>Great Britain</source>
        <translation>Großbritannien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="86"/>
        <source>Guadeloupe</source>
        <translation>Guadeloupe</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="87"/>
        <source>Guam</source>
        <translation>Guam</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="88"/>
        <source>Guatemala</source>
        <translation>Guatemala</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="89"/>
        <source>French Guiana</source>
        <translation>Französich-Guayana</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="90"/>
        <source>Guinea</source>
        <translation>Guinea</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="91"/>
        <source>Guyana</source>
        <translation>Guyana</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="92"/>
        <source>Hawaii</source>
        <translation>Hawaii</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="93"/>
        <source>Honduras</source>
        <translation>Honduras</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="94"/>
        <source>India</source>
        <translation>Indien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="95"/>
        <source>Indonesia</source>
        <translation>Indonesien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="96"/>
        <source>Iraq</source>
        <translation>Irak</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="97"/>
        <source>Iran</source>
        <translation>Iran</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="98"/>
        <source>Ireland</source>
        <translation>Irland</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="99"/>
        <source>Iceland</source>
        <translation>Island</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="100"/>
        <source>Israel</source>
        <translation>Israel</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="101"/>
        <source>Italy</source>
        <translation>Italien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="102"/>
        <source>Jamaica</source>
        <translation>Jamaika</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="103"/>
        <source>Japan</source>
        <translation>Japan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="104"/>
        <source>Yemen</source>
        <translation>Jemen</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="105"/>
        <source>Jordan</source>
        <translation>Jordanien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="106"/>
        <source>Cayman Islands</source>
        <translation>Kaiman-Inseln</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="107"/>
        <source>Cambodia</source>
        <translation>Kambodscha</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="108"/>
        <source>Cameroon</source>
        <translation>Kamerun</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="109"/>
        <source>Canada</source>
        <translation>Kanada</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="110"/>
        <source>Canary Islands</source>
        <translation>Kanarische Inseln</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="111"/>
        <source>Kazakhstan</source>
        <translation>Kasachstan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="112"/>
        <source>Qatar</source>
        <translation>Katar</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="113"/>
        <source>Kenyia</source>
        <translation>Kenia</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="114"/>
        <source>Kyrgyzstan</source>
        <translation>Kirgisien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="115"/>
        <source>Kiribati</source>
        <translation>Kiribati</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="116"/>
        <source>Colombia</source>
        <translation>Kolumbien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="117"/>
        <source>Comoros</source>
        <translation>Komoren</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="118"/>
        <source>Democratic Republic of the Congo</source>
        <translation>Demokratische Republik Kongo</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="119"/>
        <source>Republic of the Congo</source>
        <translation>Republik Kongo</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="120"/>
        <source>North Korea</source>
        <translation>Nordkorea</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="121"/>
        <source>South Korea</source>
        <translation>Südkorea</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="122"/>
        <source>Croatia</source>
        <translation>Kroatien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="123"/>
        <source>Cuba</source>
        <translation>Kuba</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="124"/>
        <source>Kuwait</source>
        <translation>Kuwait</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="125"/>
        <source>Laos</source>
        <translation>Laos</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="126"/>
        <source>Lesotho</source>
        <translation>Lesotho</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="127"/>
        <source>Latvia</source>
        <translation>Lettland</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="128"/>
        <source>Libanon</source>
        <translation>Libanon</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="129"/>
        <source>Liberia</source>
        <translation>Liberia</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="130"/>
        <source>Libya</source>
        <translation>Libyen</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="131"/>
        <source>Lithuania</source>
        <translation>Litauen</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="132"/>
        <source>Luxembourg</source>
        <translation>Luxemburg</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="133"/>
        <source>Madagascar</source>
        <translation>Madagaskar</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="134"/>
        <source>Madeira</source>
        <translation>Madeira</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="135"/>
        <source>Malawi</source>
        <translation>Malawi</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="136"/>
        <source>Malaysia</source>
        <translation>Malaysia</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="137"/>
        <source>Maldives</source>
        <translation>Malediven</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="138"/>
        <source>Mali</source>
        <translation>Mali</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="139"/>
        <source>Malta</source>
        <translation>Malta</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="140"/>
        <source>Northern Mariana Islands</source>
        <translation>Marianen</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="141"/>
        <source>Morocco</source>
        <translation>Marokko</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="142"/>
        <source>Martinique</source>
        <translation>Martinique</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="143"/>
        <source>Mauritania</source>
        <translation>Mauretanien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="144"/>
        <source>Mauritius</source>
        <translation>Mauritius</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="145"/>
        <source>Mayotte</source>
        <translation>Mayotte</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="146"/>
        <source>Macedonia</source>
        <translation>Mazedonien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="147"/>
        <source>Mexico</source>
        <translation>Mexiko</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="148"/>
        <source>Moldova</source>
        <translation>Moldawien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="149"/>
        <source>Monaco</source>
        <translation>Monaco</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="150"/>
        <source>Mongolia</source>
        <translation>Mongolei</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="151"/>
        <source>Mozambique</source>
        <translation>Mosambik</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="152"/>
        <source>Myanmar (Birma)</source>
        <translation>Myanmar (Burma)</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="153"/>
        <source>Namibia</source>
        <translation>Namibia</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="154"/>
        <source>Nepal</source>
        <translation>Nepal</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="155"/>
        <source>New Caledonia</source>
        <translation>Neukaledonien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="156"/>
        <source>New Zealand</source>
        <translation>Neuseeland</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="157"/>
        <source>Nicaragua</source>
        <translation>Nicaragua</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="158"/>
        <source>Netherlands Antilles</source>
        <translation>Niederländische Antillen</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="159"/>
        <source>Netherlands</source>
        <translation>Niederlande</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="160"/>
        <source>Niger</source>
        <translation>Niger</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="161"/>
        <source>Nigeria</source>
        <translation>Nigeria</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="162"/>
        <source>Norway</source>
        <translation>Norwegen</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="163"/>
        <source>Austria</source>
        <translation>Österreich</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="164"/>
        <source>Oman</source>
        <translation>Oman</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="165"/>
        <source>Pakistan</source>
        <translation>Pakistan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="166"/>
        <source>Palau</source>
        <translation>Palau</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="167"/>
        <source>Papua New Guinea</source>
        <translation>Papua-Neuguinea</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="168"/>
        <source>Paraguay</source>
        <translation>Paraguay</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="169"/>
        <source>Peru</source>
        <translation>Peru</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="170"/>
        <source>Philippines</source>
        <translation>Philippinen</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="171"/>
        <source>Poland</source>
        <translation>Polen</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="172"/>
        <source>French Polynesia</source>
        <translation>Französisch-Polynesien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="173"/>
        <source>Portugal</source>
        <translation>Portugal</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="174"/>
        <source>Puerto Rico</source>
        <translation>Puerto Rico</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="175"/>
        <source>Réunion</source>
        <translation>Réunion</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="176"/>
        <source>Rwanda</source>
        <translation>Ruanda</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="177"/>
        <source>Romania</source>
        <translation>Rumänien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="178"/>
        <source>Russia</source>
        <translation>Russland</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="179"/>
        <source>Solomon Islands</source>
        <translation>Solomonen</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="180"/>
        <source>Zambia</source>
        <translation>Sambia</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="181"/>
        <source>Sao Tome and Principe</source>
        <translation>Sao Tomé und Princípe</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="182"/>
        <source>Saudi Arabia</source>
        <translation>Saudi-Arabien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="183"/>
        <source>Sweden</source>
        <translation>Schweden</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="184"/>
        <source>Switzerland</source>
        <translation>Schweiz</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="185"/>
        <source>Senegal</source>
        <translation>Senegal</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="186"/>
        <source>Serbia and Montenegro</source>
        <translation>Serbien und Montenegro</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="187"/>
        <source>Seychelles</source>
        <translation>Seychellen</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="188"/>
        <source>Sierra Leone</source>
        <translation>Sierra Leone</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="189"/>
        <source>Zimbabwe</source>
        <translation>Simbabwe</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="190"/>
        <source>Singapore</source>
        <translation>Singapur</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="191"/>
        <source>Slovakia</source>
        <translation>Slowakei</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="192"/>
        <source>Slovenia</source>
        <translation>Slowenien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="193"/>
        <source>Somalia</source>
        <translation>Somalia</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="194"/>
        <source>Spain</source>
        <translation>Spanien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="195"/>
        <source>Sri Lanka</source>
        <translation>Sri Lanka</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="196"/>
        <source>St. Helena</source>
        <translation>St. Helena</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="197"/>
        <source>Saint Kitts and Nevis</source>
        <translation>St. Kitts und Nevis</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="198"/>
        <source>St. Pierre and Miquelon</source>
        <translation>St. Pierre und Miquelon</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="199"/>
        <source>Sudan</source>
        <translation>Sudan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="200"/>
        <source>South Africa</source>
        <translation>Südafrika</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="201"/>
        <source>Suriname</source>
        <translation>Surinam</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="202"/>
        <source>Swaziland</source>
        <translation>Swaziland</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="203"/>
        <source>Syria</source>
        <translation>Syrien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="204"/>
        <source>Tajikistan</source>
        <translation>Tadschikistan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="205"/>
        <source>Taiwan</source>
        <translation>Taiwan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="206"/>
        <source>United Republic of Tanzania</source>
        <translation>Tansania</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="207"/>
        <source>Thailand</source>
        <translation>Thailand</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="208"/>
        <source>Togo</source>
        <translation>Togo</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="209"/>
        <source>Tonga</source>
        <translation>Tonga</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="210"/>
        <source>Chad</source>
        <translation>Tschad</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="211"/>
        <source>Czech Republic</source>
        <translation>Tschechische Republik</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="212"/>
        <source>Turkey</source>
        <translation>Türkei</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="213"/>
        <source>Tunisia</source>
        <translation>Tunesien</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="214"/>
        <source>Turks and Caicos Islands</source>
        <translation>Turks und Caicos-Inseln</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="215"/>
        <source>Turkmenistan</source>
        <translation>Turkmenistan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="216"/>
        <source>Uganda</source>
        <translation>Uganda</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="217"/>
        <source>Ukraine</source>
        <translation>Ukraine</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="218"/>
        <source>United Nations</source>
        <translation>Vereinte Nationen (UN)</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="219"/>
        <source>Hungary</source>
        <translation>Ungarn</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="220"/>
        <source>Uruguay</source>
        <translation>Uruguay</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="221"/>
        <source>United States of America (USA)</source>
        <translation>Vereinigte Staaten von Amerika (USA)</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="222"/>
        <source>Uzbekistan</source>
        <translation>Usbekistan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="223"/>
        <source>Vanuatu</source>
        <translation>Vanatu</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="224"/>
        <source>Vatican</source>
        <translation>Vatikan</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="225"/>
        <source>Venezuela</source>
        <translation>Venezuela</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="226"/>
        <source>United Arab Emirates</source>
        <translation>Vereinigte Arabische Emirate</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="227"/>
        <source>Vietnam</source>
        <translation>Vietnam</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="228"/>
        <source>Wallis and Futuna Islands</source>
        <translation>Wallis und Fortuna</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="229"/>
        <source>Central African Republic</source>
        <translation>Zentralafrikanische Republik</translation>
    </message>
    <message>
        <location filename="../src/data/itu.cpp" line="230"/>
        <source>Cyprus</source>
        <translation>Zypern</translation>
    </message>
</context>
<context>
    <name>@default</name>
    <message>
        <source>Egypt</source>
        <translation type="obsolete">Ägypten</translation>
    </message>
    <message>
        <source>Equatorial Guinea</source>
        <translation type="obsolete">Äquatorial-Guinea</translation>
    </message>
    <message>
        <source>Ethiopia</source>
        <translation type="obsolete">Äthiopien</translation>
    </message>
    <message>
        <source>Afghanistan</source>
        <translation type="obsolete">Afghanistan</translation>
    </message>
    <message>
        <source>Alaska</source>
        <translation type="obsolete">Alaska</translation>
    </message>
    <message>
        <source>Albania</source>
        <translation type="obsolete">Albanien</translation>
    </message>
    <message>
        <source>Algeria</source>
        <translation type="obsolete">Algerien</translation>
    </message>
    <message>
        <source>Andorra</source>
        <translation type="obsolete">Andorra</translation>
    </message>
    <message>
        <source>Angola</source>
        <translation type="obsolete">Angola</translation>
    </message>
    <message>
        <source>Anguilla</source>
        <translation type="obsolete">Anguilla</translation>
    </message>
    <message>
        <source>Antarctica</source>
        <translation type="obsolete">Antarktis</translation>
    </message>
    <message>
        <source>Antigua</source>
        <translation type="obsolete">Antigua</translation>
    </message>
    <message>
        <source>Argentina</source>
        <translation type="obsolete">Argentinien</translation>
    </message>
    <message>
        <source>Armenia</source>
        <translation type="obsolete">Armenien</translation>
    </message>
    <message>
        <source>Ascension</source>
        <translation type="obsolete">Ascension</translation>
    </message>
    <message>
        <source>Azerbaijan</source>
        <translation type="obsolete">Aserbaidschan</translation>
    </message>
    <message>
        <source>Australia</source>
        <translation type="obsolete">Australien</translation>
    </message>
    <message>
        <source>Azores</source>
        <translation type="obsolete">Azoren</translation>
    </message>
    <message>
        <source>Bahamas</source>
        <translation type="obsolete">Bahamas</translation>
    </message>
    <message>
        <source>Bahrain</source>
        <translation type="obsolete">Bahrain</translation>
    </message>
    <message>
        <source>Bangladesh</source>
        <translation type="obsolete">Bangladesch</translation>
    </message>
    <message>
        <source>Barbados</source>
        <translation type="obsolete">Barbados</translation>
    </message>
    <message>
        <source>Belarus</source>
        <translation type="obsolete">Belarus (Weißrussland)</translation>
    </message>
    <message>
        <source>Belgium</source>
        <translation type="obsolete">Belgien</translation>
    </message>
    <message>
        <source>Belize</source>
        <translation type="obsolete">Belize</translation>
    </message>
    <message>
        <source>Benin</source>
        <translation type="obsolete">Benin</translation>
    </message>
    <message>
        <source>Bhutan</source>
        <translation type="obsolete">Bhutan</translation>
    </message>
    <message>
        <source>Bolivia</source>
        <translation type="obsolete">Bolivien</translation>
    </message>
    <message>
        <source>Bosnia and Herzegovina</source>
        <translation type="obsolete">Bosnien-Herzegowina</translation>
    </message>
    <message>
        <source>Botswana</source>
        <translation type="obsolete">Botswana</translation>
    </message>
    <message>
        <source>Brasilia</source>
        <translation type="obsolete">Brasilien</translation>
    </message>
    <message>
        <source>Brunei</source>
        <translation type="obsolete">Brunei</translation>
    </message>
    <message>
        <source>Bulgaria</source>
        <translation type="obsolete">Bulgarien</translation>
    </message>
    <message>
        <source>Burkina Faso</source>
        <translation type="obsolete">Burkina Faso</translation>
    </message>
    <message>
        <source>Burundi</source>
        <translation type="obsolete">Burundi</translation>
    </message>
    <message>
        <source>Chile</source>
        <translation type="obsolete">Chile</translation>
    </message>
    <message>
        <source>China</source>
        <translation type="obsolete">China</translation>
    </message>
    <message>
        <source>Costa Rica</source>
        <translation type="obsolete">Costa Rica</translation>
    </message>
    <message>
        <source>Cote d&apos;Ivoire</source>
        <translation type="obsolete">Cote d&apos;Ivoire (Elfenbeinküste)</translation>
    </message>
    <message>
        <source>Denmark</source>
        <translation type="obsolete">Dänemark</translation>
    </message>
    <message>
        <source>Germany</source>
        <translation type="obsolete">Deutschland</translation>
    </message>
    <message>
        <source>Diego García</source>
        <translation type="obsolete">Diego García</translation>
    </message>
    <message>
        <source>Dominica</source>
        <translation type="obsolete">Dominica</translation>
    </message>
    <message>
        <source>Dominican Republic</source>
        <translation type="obsolete">Dominikanische Republik</translation>
    </message>
    <message>
        <source>Dschibuti</source>
        <translation type="obsolete">Dschibuti</translation>
    </message>
    <message>
        <source>Ecuador</source>
        <translation type="obsolete">Ecuador</translation>
    </message>
    <message>
        <source>El Salvador</source>
        <translation type="obsolete">El Salvador</translation>
    </message>
    <message>
        <source>Eritrea</source>
        <translation type="obsolete">Eritrea</translation>
    </message>
    <message>
        <source>Estonia</source>
        <translation type="obsolete">Estland</translation>
    </message>
    <message>
        <source>Faroe islands</source>
        <translation type="obsolete">Faroer-Inseln</translation>
    </message>
    <message>
        <source>Fiji</source>
        <translation type="obsolete">Fidschi</translation>
    </message>
    <message>
        <source>Finland</source>
        <translation type="obsolete">Finnland</translation>
    </message>
    <message>
        <source>France</source>
        <translation type="obsolete">Frankreich</translation>
    </message>
    <message>
        <source>Gabun</source>
        <translation type="obsolete">Gabun</translation>
    </message>
    <message>
        <source>Gambia</source>
        <translation type="obsolete">Gambia</translation>
    </message>
    <message>
        <source>Georgia</source>
        <translation type="obsolete">Georgien</translation>
    </message>
    <message>
        <source>Ghana</source>
        <translation type="obsolete">Ghana</translation>
    </message>
    <message>
        <source>Gibraltar</source>
        <translation type="obsolete">Gibraltar</translation>
    </message>
    <message>
        <source>Grenada</source>
        <translation type="obsolete">Grenada</translation>
    </message>
    <message>
        <source>Greece</source>
        <translation type="obsolete">Griechenland</translation>
    </message>
    <message>
        <source>Greenland</source>
        <translation type="obsolete">Grönland</translation>
    </message>
    <message>
        <source>Great Britain</source>
        <translation type="obsolete">Großbritannien</translation>
    </message>
    <message>
        <source>Guadeloupe</source>
        <translation type="obsolete">Guadeloupe</translation>
    </message>
    <message>
        <source>Guam</source>
        <translation type="obsolete">Guam</translation>
    </message>
    <message>
        <source>Guatemala</source>
        <translation type="obsolete">Guatemala</translation>
    </message>
    <message>
        <source>French Guiana</source>
        <translation type="obsolete">Französich-Guayana</translation>
    </message>
    <message>
        <source>Guinea</source>
        <translation type="obsolete">Guinea</translation>
    </message>
    <message>
        <source>Guyana</source>
        <translation type="obsolete">Guyana</translation>
    </message>
    <message>
        <source>Hawaii</source>
        <translation type="obsolete">Hawaii</translation>
    </message>
    <message>
        <source>Honduras</source>
        <translation type="obsolete">Honduras</translation>
    </message>
    <message>
        <source>India</source>
        <translation type="obsolete">Indien</translation>
    </message>
    <message>
        <source>Indonesia</source>
        <translation type="obsolete">Indonesien</translation>
    </message>
    <message>
        <source>Iraq</source>
        <translation type="obsolete">Irak</translation>
    </message>
    <message>
        <source>Iran</source>
        <translation type="obsolete">Iran</translation>
    </message>
    <message>
        <source>Ireland</source>
        <translation type="obsolete">Irland</translation>
    </message>
    <message>
        <source>Iceland</source>
        <translation type="obsolete">Island</translation>
    </message>
    <message>
        <source>Israel</source>
        <translation type="obsolete">Israel</translation>
    </message>
    <message>
        <source>Italy</source>
        <translation type="obsolete">Italien</translation>
    </message>
    <message>
        <source>Jamaica</source>
        <translation type="obsolete">Jamaika</translation>
    </message>
    <message>
        <source>Japan</source>
        <translation type="obsolete">Japan</translation>
    </message>
    <message>
        <source>Yemen</source>
        <translation type="obsolete">Jemen</translation>
    </message>
    <message>
        <source>Jordan</source>
        <translation type="obsolete">Jordanien</translation>
    </message>
    <message>
        <source>Cayman Islands</source>
        <translation type="obsolete">Kaiman-Inseln</translation>
    </message>
    <message>
        <source>Cambodia</source>
        <translation type="obsolete">Kambodscha</translation>
    </message>
    <message>
        <source>Cameroon</source>
        <translation type="obsolete">Kamerun</translation>
    </message>
    <message>
        <source>Canada</source>
        <translation type="obsolete">Kanada</translation>
    </message>
    <message>
        <source>Canary Islands</source>
        <translation type="obsolete">Kanarische Inseln</translation>
    </message>
    <message>
        <source>Kazakhstan</source>
        <translation type="obsolete">Kasachstan</translation>
    </message>
    <message>
        <source>Qatar</source>
        <translation type="obsolete">Katar</translation>
    </message>
    <message>
        <source>Kenyia</source>
        <translation type="obsolete">Kenia</translation>
    </message>
    <message>
        <source>Kyrgyzstan</source>
        <translation type="obsolete">Kirgisien</translation>
    </message>
    <message>
        <source>Kiribati</source>
        <translation type="obsolete">Kiribati</translation>
    </message>
    <message>
        <source>Colombia</source>
        <translation type="obsolete">Kolumbien</translation>
    </message>
    <message>
        <source>Comoros</source>
        <translation type="obsolete">Komoren</translation>
    </message>
    <message>
        <source>Democratic Republic of the Congo</source>
        <translation type="obsolete">Demokratische Republik Kongo</translation>
    </message>
    <message>
        <source>Republic of the Congo</source>
        <translation type="obsolete">Republik Kongo</translation>
    </message>
    <message>
        <source>North Korea</source>
        <translation type="obsolete">Nordkorea</translation>
    </message>
    <message>
        <source>South Korea</source>
        <translation type="obsolete">Südkorea</translation>
    </message>
    <message>
        <source>Croatia</source>
        <translation type="obsolete">Kroatien</translation>
    </message>
    <message>
        <source>Cuba</source>
        <translation type="obsolete">Kuba</translation>
    </message>
    <message>
        <source>Kuwait</source>
        <translation type="obsolete">Kuwait</translation>
    </message>
    <message>
        <source>Laos</source>
        <translation type="obsolete">Laos</translation>
    </message>
    <message>
        <source>Lesotho</source>
        <translation type="obsolete">Lesotho</translation>
    </message>
    <message>
        <source>Latvia</source>
        <translation type="obsolete">Lettland</translation>
    </message>
    <message>
        <source>Libanon</source>
        <translation type="obsolete">Libanon</translation>
    </message>
    <message>
        <source>Liberia</source>
        <translation type="obsolete">Liberia</translation>
    </message>
    <message>
        <source>Libya</source>
        <translation type="obsolete">Libyen</translation>
    </message>
    <message>
        <source>Lithuania</source>
        <translation type="obsolete">Litauen</translation>
    </message>
    <message>
        <source>Luxembourg</source>
        <translation type="obsolete">Luxemburg</translation>
    </message>
    <message>
        <source>Madagascar</source>
        <translation type="obsolete">Madagaskar</translation>
    </message>
    <message>
        <source>Madeira</source>
        <translation type="obsolete">Madeira</translation>
    </message>
    <message>
        <source>Malawi</source>
        <translation type="obsolete">Malawi</translation>
    </message>
    <message>
        <source>Malaysia</source>
        <translation type="obsolete">Malaysia</translation>
    </message>
    <message>
        <source>Maldives</source>
        <translation type="obsolete">Malediven</translation>
    </message>
    <message>
        <source>Mali</source>
        <translation type="obsolete">Mali</translation>
    </message>
    <message>
        <source>Malta</source>
        <translation type="obsolete">Malta</translation>
    </message>
    <message>
        <source>Northern Mariana Islands</source>
        <translation type="obsolete">Marianen</translation>
    </message>
    <message>
        <source>Morocco</source>
        <translation type="obsolete">Marokko</translation>
    </message>
    <message>
        <source>Martinique</source>
        <translation type="obsolete">Martinique</translation>
    </message>
    <message>
        <source>Mauritania</source>
        <translation type="obsolete">Mauretanien</translation>
    </message>
    <message>
        <source>Mauritius</source>
        <translation type="obsolete">Mauritius</translation>
    </message>
    <message>
        <source>Mayotte</source>
        <translation type="obsolete">Mayotte</translation>
    </message>
    <message>
        <source>Macedonia</source>
        <translation type="obsolete">Mazedonien</translation>
    </message>
    <message>
        <source>Mexico</source>
        <translation type="obsolete">Mexiko</translation>
    </message>
    <message>
        <source>Moldova</source>
        <translation type="obsolete">Moldawien</translation>
    </message>
    <message>
        <source>Monaco</source>
        <translation type="obsolete">Monaco</translation>
    </message>
    <message>
        <source>Mozambique</source>
        <translation type="obsolete">Mosambik</translation>
    </message>
    <message>
        <source>Myanmar (Birma)</source>
        <translation type="obsolete">Myanmar (Burma)</translation>
    </message>
    <message>
        <source>Namibia</source>
        <translation type="obsolete">Namibia</translation>
    </message>
    <message>
        <source>Nepal</source>
        <translation type="obsolete">Nepal</translation>
    </message>
    <message>
        <source>New Caledonia</source>
        <translation type="obsolete">Neukaledonien</translation>
    </message>
    <message>
        <source>New Zealand</source>
        <translation type="obsolete">Neuseeland</translation>
    </message>
    <message>
        <source>Nicaragua</source>
        <translation type="obsolete">Nicaragua</translation>
    </message>
    <message>
        <source>Netherlands Antilles</source>
        <translation type="obsolete">Niederländische Antillen</translation>
    </message>
    <message>
        <source>Netherlands</source>
        <translation type="obsolete">Niederlande</translation>
    </message>
    <message>
        <source>Niger</source>
        <translation type="obsolete">Niger</translation>
    </message>
    <message>
        <source>Nigeria</source>
        <translation type="obsolete">Nigeria</translation>
    </message>
    <message>
        <source>Norway</source>
        <translation type="obsolete">Norwegen</translation>
    </message>
    <message>
        <source>Austria</source>
        <translation type="obsolete">Österreich</translation>
    </message>
    <message>
        <source>Oman</source>
        <translation type="obsolete">Oman</translation>
    </message>
    <message>
        <source>Pakistan</source>
        <translation type="obsolete">Pakistan</translation>
    </message>
    <message>
        <source>Palau</source>
        <translation type="obsolete">Palau</translation>
    </message>
    <message>
        <source>Papua New Guinea</source>
        <translation type="obsolete">Papua-Neuguinea</translation>
    </message>
    <message>
        <source>Paraguay</source>
        <translation type="obsolete">Paraguay</translation>
    </message>
    <message>
        <source>Peru</source>
        <translation type="obsolete">Peru</translation>
    </message>
    <message>
        <source>Philippines</source>
        <translation type="obsolete">Philippinen</translation>
    </message>
    <message>
        <source>Poland</source>
        <translation type="obsolete">Polen</translation>
    </message>
    <message>
        <source>French Polynesia</source>
        <translation type="obsolete">Französisch-Polynesien</translation>
    </message>
    <message>
        <source>Portugal</source>
        <translation type="obsolete">Portugal</translation>
    </message>
    <message>
        <source>Puerto Rico</source>
        <translation type="obsolete">Puerto Rico</translation>
    </message>
    <message>
        <source>Réunion</source>
        <translation type="obsolete">Réunion</translation>
    </message>
    <message>
        <source>Rwanda</source>
        <translation type="obsolete">Ruanda</translation>
    </message>
    <message>
        <source>Romania</source>
        <translation type="obsolete">Rumänien</translation>
    </message>
    <message>
        <source>Russia</source>
        <translation type="obsolete">Russland</translation>
    </message>
    <message>
        <source>Solomon Islands</source>
        <translation type="obsolete">Solomonen</translation>
    </message>
    <message>
        <source>Zambia</source>
        <translation type="obsolete">Sambia</translation>
    </message>
    <message>
        <source>Sao Tome and Principe</source>
        <translation type="obsolete">Sao Tomé und Princípe</translation>
    </message>
    <message>
        <source>Saudi Arabia</source>
        <translation type="obsolete">Saudi-Arabien</translation>
    </message>
    <message>
        <source>Sweden</source>
        <translation type="obsolete">Schweden</translation>
    </message>
    <message>
        <source>Switzerland</source>
        <translation type="obsolete">Schweiz</translation>
    </message>
    <message>
        <source>Senegal</source>
        <translation type="obsolete">Senegal</translation>
    </message>
    <message>
        <source>Serbia and Montenegro</source>
        <translation type="obsolete">Serbien und Montenegro</translation>
    </message>
    <message>
        <source>Seychelles</source>
        <translation type="obsolete">Seychellen</translation>
    </message>
    <message>
        <source>Sierra Leone</source>
        <translation type="obsolete">Sierra Leone</translation>
    </message>
    <message>
        <source>Zimbabwe</source>
        <translation type="obsolete">Simbabwe</translation>
    </message>
    <message>
        <source>Singapore</source>
        <translation type="obsolete">Singapur</translation>
    </message>
    <message>
        <source>Slovakia</source>
        <translation type="obsolete">Slowakei</translation>
    </message>
    <message>
        <source>Slovenia</source>
        <translation type="obsolete">Slowenien</translation>
    </message>
    <message>
        <source>Somalia</source>
        <translation type="obsolete">Somalia</translation>
    </message>
    <message>
        <source>Spain</source>
        <translation type="obsolete">Spanien</translation>
    </message>
    <message>
        <source>Sri Lanka</source>
        <translation type="obsolete">Sri Lanka</translation>
    </message>
    <message>
        <source>St. Helena</source>
        <translation type="obsolete">St. Helena</translation>
    </message>
    <message>
        <source>Saint Kitts and Nevis</source>
        <translation type="obsolete">St. Kitts und Nevis</translation>
    </message>
    <message>
        <source>St. Pierre and Miquelon</source>
        <translation type="obsolete">St. Pierre und Miquelon</translation>
    </message>
    <message>
        <source>Sudan</source>
        <translation type="obsolete">Sudan</translation>
    </message>
    <message>
        <source>South Africa</source>
        <translation type="obsolete">Südafrika</translation>
    </message>
    <message>
        <source>Suriname</source>
        <translation type="obsolete">Surinam</translation>
    </message>
    <message>
        <source>Swaziland</source>
        <translation type="obsolete">Swaziland</translation>
    </message>
    <message>
        <source>Syria</source>
        <translation type="obsolete">Syrien</translation>
    </message>
    <message>
        <source>Tajikistan</source>
        <translation type="obsolete">Tadschikistan</translation>
    </message>
    <message>
        <source>Taiwan</source>
        <translation type="obsolete">Taiwan</translation>
    </message>
    <message>
        <source>United Republic of Tanzania</source>
        <translation type="obsolete">Tansania</translation>
    </message>
    <message>
        <source>Thailand</source>
        <translation type="obsolete">Thailand</translation>
    </message>
    <message>
        <source>Togo</source>
        <translation type="obsolete">Togo</translation>
    </message>
    <message>
        <source>Tonga</source>
        <translation type="obsolete">Tonga</translation>
    </message>
    <message>
        <source>Chad</source>
        <translation type="obsolete">Tschad</translation>
    </message>
    <message>
        <source>Czech Republic</source>
        <translation type="obsolete">Tschechische Republik</translation>
    </message>
    <message>
        <source>Turkey</source>
        <translation type="obsolete">Türkei</translation>
    </message>
    <message>
        <source>Tunisia</source>
        <translation type="obsolete">Tunesien</translation>
    </message>
    <message>
        <source>Turks and Caicos Islands</source>
        <translation type="obsolete">Turks und Caicos-Inseln</translation>
    </message>
    <message>
        <source>Turkmenistan</source>
        <translation type="obsolete">Turkmenistan</translation>
    </message>
    <message>
        <source>Uganda</source>
        <translation type="obsolete">Uganda</translation>
    </message>
    <message>
        <source>Ukraine</source>
        <translation type="obsolete">Ukraine</translation>
    </message>
    <message>
        <source>United Nations</source>
        <translation type="obsolete">Vereinte Nationen (UN)</translation>
    </message>
    <message>
        <source>Hungary</source>
        <translation type="obsolete">Ungarn</translation>
    </message>
    <message>
        <source>Uruguay</source>
        <translation type="obsolete">Uruguay</translation>
    </message>
    <message>
        <source>United States of America (USA)</source>
        <translation type="obsolete">Vereinigte Staaten von Amerika (USA)</translation>
    </message>
    <message>
        <source>Uzbekistan</source>
        <translation type="obsolete">Usbekistan</translation>
    </message>
    <message>
        <source>Vanuatu</source>
        <translation type="obsolete">Vanatu</translation>
    </message>
    <message>
        <source>Vatican</source>
        <translation type="obsolete">Vatikan</translation>
    </message>
    <message>
        <source>Venezuela</source>
        <translation type="obsolete">Venezuela</translation>
    </message>
    <message>
        <source>United Arab Emirates</source>
        <translation type="obsolete">Vereinigte Arabische Emirate</translation>
    </message>
    <message>
        <source>Vietnam</source>
        <translation type="obsolete">Vietnam</translation>
    </message>
    <message>
        <source>Wallis and Futuna Islands</source>
        <translation type="obsolete">Wallis und Fortuna</translation>
    </message>
    <message>
        <source>Central African Republic</source>
        <translation type="obsolete">Zentralafrikanische Republik</translation>
    </message>
    <message>
        <source>Cyprus</source>
        <translation type="obsolete">Zypern</translation>
    </message>
    <message>
        <source>Mongolia</source>
        <translation type="obsolete">Mongolei</translation>
    </message>
</context>
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../src/aboutdialog.cpp" line="25"/>
        <source>About HFP</source>
        <translation>Über HFP</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="42"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="69"/>
        <source>&lt;p&gt;&lt;nobr&gt;This is a station schedule managing tool for Unix and MacOS X&lt;/nobr&gt; written in C++ using the Qt library.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Thanks to:&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Trolltech for the Qt library&lt;/li&gt;&lt;li&gt;ADDX e.V. (&lt;tt&gt;http://www.addx.de&lt;/tt&gt;) for providing the schedules&lt;/li&gt;&lt;li&gt;Gtk+ artists for the nice stock icons&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;tt&gt;http://www.bwalle.de/hfp/&lt;/tt&gt;&lt;/p&gt;</source>
        <translation>&lt;p&gt;&lt;nobr&gt;Diest ist ein Hörfahrplanprogramm für Unix und MacOS X&lt;/nobr&gt;. Es wurde in C++ mit Hilfe der Qt-Bibliothek geschrieben.&lt;/p&gt;&lt;p&gt;&lt;b&gt;Dank an:&lt;/b&gt;&lt;ul&gt;&lt;li&gt;Trolltech für die Qt-Bibliothek&lt;/li&gt;&lt;li&gt;ADDX e.V. (&lt;tt&gt;http://www.addx.de&lt;/tt&gt;) für die Hörfahrpläne&lt;/li&gt;&lt;li&gt;Gtk+-Grafiker für die schönen Icons&lt;/li&gt;&lt;/ul&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Homepage:&lt;/b&gt; &lt;tt&gt;http://www.bwalle.de/hfp/&lt;/tt&gt;&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="79"/>
        <source>&amp;About</source>
        <translation>&amp;Über</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="100"/>
        <source>&amp;License</source>
        <translation>&amp;Lizenz</translation>
    </message>
    <message>
        <location filename="../src/aboutdialog.cpp" line="31"/>
        <source>&lt;p&gt;&lt;b&gt;HFP </source>
        <translation>&lt;p&gt;&lt;b&gt;HFP</translation>
    </message>
</context>
<context>
    <name>FilterPanel</name>
    <message>
        <location filename="../src/filterpanel.cpp" line="29"/>
        <source>Languages:</source>
        <translation>Sprachen:</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="31"/>
        <source>&amp;German</source>
        <translation>De&amp;utsch</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="32"/>
        <source>&amp;English</source>
        <translation>&amp;Englisch</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="33"/>
        <source>&amp;French</source>
        <translation>&amp;Französisch</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="34"/>
        <source>&amp;Spanish</source>
        <translation>&amp;Spanisch</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="35"/>
        <source>&amp;Russian</source>
        <translation>&amp;Russisch</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="36"/>
        <source>&amp;Turkish</source>
        <translation>&amp;Türkisch</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="55"/>
        <source>All</source>
        <translation>Alle</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="56"/>
        <source>Is active now</source>
        <translation>Gerade aktiv</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="57"/>
        <source>Is active at</source>
        <translation>Aktiv um</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="58"/>
        <source>Begins at</source>
        <translation>Startet um</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="59"/>
        <source>Ends at</source>
        <translation>Endet um</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="42"/>
        <source>T&amp;ime:</source>
        <translation>Ze&amp;it:</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="73"/>
        <source>St&amp;ations:</source>
        <translation>Se&amp;nder:</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="81"/>
        <source>Fre&amp;quencies:</source>
        <translation>Fre&amp;quenzen:</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="261"/>
        <source>The regular expression for the station is not valid:

%1</source>
        <translation>Der reguläre Ausdruck für den Sender ist nicht gültig:

%1</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="271"/>
        <source>The regular expression for the frequencies is not valid:

%1</source>
        <translation>Der reguläre Ausdruck für die Frequenzen ist nicht gültig:

%1</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="46"/>
        <source>Toleran&amp;ce:</source>
        <translation>Toleran&amp;z:</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="68"/>
        <location filename="../src/filterpanel.cpp" line="76"/>
        <location filename="../src/filterpanel.cpp" line="84"/>
        <source>Negate</source>
        <translation>Negieren</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="65"/>
        <source>C&amp;ountry:</source>
        <translation>La&amp;nd:</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="281"/>
        <source>The regular expression for the country is not valid:

%1</source>
        <translation>Der reguläre Ausdruck für das Land ist nicht gültig:

%1</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="97"/>
        <source>Apply filter</source>
        <translation>Filter anwenden</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="103"/>
        <source>Save as favourite</source>
        <translation>Als Favorit speichern</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="109"/>
        <source>Restore favourite</source>
        <translation>Favorit laden</translation>
    </message>
    <message>
        <location filename="../src/filterpanel.cpp" line="115"/>
        <source>Clear filter entries</source>
        <translation>Filtereinträge zurücksetzen</translation>
    </message>
</context>
<context>
    <name>Hfp</name>
    <message>
        <location filename="../src/hfp.cpp" line="99"/>
        <source>Application</source>
        <translation>Anwendung</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="111"/>
        <source>Clock</source>
        <translation>Uhr</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="122"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="142"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="293"/>
        <source>E&amp;xit</source>
        <translation>B&amp;eenden</translation>
    </message>
    <message>
        <source>&amp;What&apos;s this</source>
        <translation type="obsolete">&amp;Kontexthilfe</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="325"/>
        <source>&amp;About...</source>
        <translation>Ü&amp;ber...</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="326"/>
        <source>About &amp;Qt...</source>
        <translation>Über &amp;Qt...</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="218"/>
        <source>HFP - Programme Schedule Program</source>
        <translation>HFP - Hörfahrplanprogramm</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="224"/>
        <source>page</source>
        <translation>Seite</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="39"/>
        <source>HFP - Frequency Schedule Program</source>
        <translation>HFP - Hörfahrplanprogramm</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="167"/>
        <source>Text files (*.txt)</source>
        <translation>Textdateien (*.txt)</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="168"/>
        <source>HFP - Choose a file</source>
        <translation>HFP - Wählen Sie eine Datei</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="175"/>
        <source>Could not open the specified file for writing:

%1</source>
        <translation>Datei konnte nicht zum Schreiben geöffnet werden:

%1</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="298"/>
        <source>&amp;Reload data</source>
        <translation>Daten &amp;neu laden</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="303"/>
        <source>&amp;Export...</source>
        <translation>E&amp;xportieren...</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="308"/>
        <source>&amp;Update data...</source>
        <translation>Daten &amp;aktualisieren...</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="317"/>
        <source>&amp;Print...</source>
        <translation>&amp;Drucken...</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="135"/>
        <source>&amp;Tools</source>
        <translation>&amp;Extras</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="321"/>
        <source>&amp;Country codes...</source>
        <translation>&amp;Ländercodes...</translation>
    </message>
    <message>
        <location filename="../src/hfp.cpp" line="310"/>
        <source>&amp;Automatic update</source>
        <translation>&amp;Automatische Aktualisierung</translation>
    </message>
</context>
<context>
    <name>ItuDialog</name>
    <message>
        <location filename="../src/itudialog.cpp" line="27"/>
        <source>ITU Countries</source>
        <translation>ITU Ländercodes</translation>
    </message>
    <message>
        <location filename="../src/itudialog.cpp" line="32"/>
        <source>&amp;Filter:</source>
        <translation>&amp;Filter:</translation>
    </message>
    <message>
        <location filename="../src/itudialog.cpp" line="38"/>
        <source>ITU</source>
        <translation>ITU</translation>
    </message>
    <message>
        <location filename="../src/itudialog.cpp" line="39"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="../src/itudialog.cpp" line="46"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="../src/itudialog.cpp" line="83"/>
        <source>The regular expression for the country is not valid:

%1</source>
        <translation>Der reguläre Ausdruck für das Land ist nicht gültig:

%1</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/data/transmission.cpp" line="132"/>
        <source>German</source>
        <translation>Deutsch</translation>
    </message>
    <message>
        <location filename="../src/data/transmission.cpp" line="134"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="../src/data/transmission.cpp" line="136"/>
        <source>French</source>
        <translation>Französisch</translation>
    </message>
    <message>
        <location filename="../src/data/transmission.cpp" line="138"/>
        <source>Spanish</source>
        <translation>Spanisch</translation>
    </message>
    <message>
        <location filename="../src/data/transmission.cpp" line="140"/>
        <source>Turkish</source>
        <translation>Türkisch</translation>
    </message>
    <message>
        <location filename="../src/data/transmission.cpp" line="142"/>
        <source>Russian</source>
        <translation>Russisch</translation>
    </message>
    <message>
        <location filename="../src/data/transmission.cpp" line="144"/>
        <source>Unknown</source>
        <translation>Unbekannt</translation>
    </message>
    <message>
        <location filename="../src/data/transmissionaddxreader.cpp" line="34"/>
        <location filename="../src/data/transmissiontextreader.cpp" line="36"/>
        <source>The file %1 does not exist.</source>
        <translation>Die Datei %1 existiert nicht.</translation>
    </message>
    <message>
        <location filename="../src/data/transmissionreader.cpp" line="66"/>
        <source>Directory %1 does not exist.</source>
        <translation>Das Verzeichnis %1 existiert nicht.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="93"/>
        <location filename="../src/main.cpp" line="97"/>
        <source>HFP</source>
        <translation>HFP</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="94"/>
        <source>No more memory available. The application
will be closed.</source>
        <translation>Kein Speicher mehr verfügbar. Die Anwendung wird beendet.</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="98"/>
        <source>An unknown error occurred:
%1
The application will be closed.</source>
        <translation>Unbekannter Fehler aufgetrten:
%1
Die Anwendung wird beenet.</translation>
    </message>
</context>
<context>
    <name>StationList</name>
    <message>
        <location filename="../src/stationlist.cpp" line="40"/>
        <location filename="../src/stationlist.cpp" line="163"/>
        <location filename="../src/stationlist.cpp" line="195"/>
        <source>Start</source>
        <translation>Beginn</translation>
    </message>
    <message>
        <location filename="../src/stationlist.cpp" line="41"/>
        <location filename="../src/stationlist.cpp" line="164"/>
        <location filename="../src/stationlist.cpp" line="196"/>
        <source>End</source>
        <translation>Ende</translation>
    </message>
    <message>
        <location filename="../src/stationlist.cpp" line="42"/>
        <location filename="../src/stationlist.cpp" line="165"/>
        <location filename="../src/stationlist.cpp" line="197"/>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <location filename="../src/stationlist.cpp" line="43"/>
        <location filename="../src/stationlist.cpp" line="166"/>
        <location filename="../src/stationlist.cpp" line="198"/>
        <source>ITU</source>
        <translation>Land</translation>
    </message>
    <message>
        <location filename="../src/stationlist.cpp" line="44"/>
        <location filename="../src/stationlist.cpp" line="167"/>
        <location filename="../src/stationlist.cpp" line="199"/>
        <source>Station</source>
        <translation>Sender</translation>
    </message>
    <message>
        <location filename="../src/stationlist.cpp" line="45"/>
        <location filename="../src/stationlist.cpp" line="168"/>
        <location filename="../src/stationlist.cpp" line="200"/>
        <source>Frequencies</source>
        <translation>Frequenzen</translation>
    </message>
    <message>
        <location filename="../src/stationlist.cpp" line="135"/>
        <source>An unknown error occured while reading the data files:

%1</source>
        <translation>Ein unbekannter Fehler trat während des Lesens der Datendateien auf:

%1</translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <location filename="../src/updatedialog.cpp" line="26"/>
        <source>HFP - Update</source>
        <translation>HFP - Daten aktualisieren</translation>
    </message>
    <message>
        <location filename="../src/updatedialog.cpp" line="28"/>
        <source>Last update:</source>
        <translation>Datenstand:</translation>
    </message>
    <message>
        <location filename="../src/updatedialog.cpp" line="32"/>
        <source>Address:</source>
        <translation>Adresse:</translation>
    </message>
    <message>
        <location filename="../src/updatedialog.cpp" line="37"/>
        <source>Progress:</source>
        <translation>Fortschritt:</translation>
    </message>
    <message>
        <location filename="../src/updatedialog.cpp" line="44"/>
        <source>Start &amp;update</source>
        <translation>&amp;Aktualisierung starten</translation>
    </message>
    <message>
        <location filename="../src/updatedialog.cpp" line="45"/>
        <source>&amp;Close</source>
        <translation>&amp;Schließen</translation>
    </message>
    <message>
        <location filename="../src/updatedialog.cpp" line="131"/>
        <source>Failed to create the directory %1. Check permissions.</source>
        <translation>Verzeichnis %1 konnte nicht erstellt werden. Überprüfen Sie die Berechtigungen.</translation>
    </message>
    <message>
        <location filename="../src/updatedialog.cpp" line="148"/>
        <location filename="../src/updatedialog.cpp" line="163"/>
        <source>Failed to download the data file:

%1</source>
        <translation>Die Datendatei konnte nicht heruntergeladen werden:

%1</translation>
    </message>
    <message>
        <source>Failed to run unzip. Probably, unzip is not installed or it is not in $PATH.</source>
        <translation type="obsolete">Das Programm unzip konnte nicht gestartet werden. Vielleicht ist unzip nicht
installiert oder nicht im $PATH.</translation>
    </message>
    <message>
        <location filename="../src/updatedialog.cpp" line="180"/>
        <location filename="../src/updatedialog.cpp" line="193"/>
        <source>Failed to extract file %1.</source>
        <translation>Die Datei %1 konnte nicht entpackt werden.</translation>
    </message>
</context>
</TS>
